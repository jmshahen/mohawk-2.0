/********************************************************************
 * PROGRAM NAME: Mohawk - A Tool for Finding Errors in ARBAC Policies	
 *		
 * AUTHORS: Karthick Jayaraman
 *	
 * BEGIN DATE: October, 2009
 *
 * LICENSE: Please view LICENSE file in the home dir of this Program
 ********************************************************************/

Installation, configuration and use of Mohawk tool is very
straightforward. Mohawk has been primarily tested on Linux and Mac
OS. Therefore, our installation instructions assume you are using
Linux or Mac systems. We will test and release a windows version at a
later time.

Here are the steps to use Moahawk:

1. Install NuSMV 2.6. You can obtain the NuSMV source code / binary from
following site: http://nusmv.irst.itc.it/. 

2. The Mohawk tool requires the latest version of JVM installed.

3. Mohawk is a Java-based tool and is available as a jar file. You may
either download the jar file from the website or build the jar file
using the sources.

Building jar file from sources:
	 3.1 Compile the Java sources and create the jar file using
	 the following ANT command : "ant dist". The build process
	 creates the jar file in the directory work/dist. 

The Mohawk tool can be run from commandline as follows (after
completing step (1) and (2) ).

    java -cp ./mohawk.jar mohawk.AbsRefine  <ARBAC SPECFile> 

If Mohawk finds an error, Mohawk displays the counter example from
NuSMV in the console. Mohawk writes the ARBAC spec used in each
refinement step to a file with the following naming convention:
rbacinstance<fileinstance>. Therefore, rbacinstance1 contains the
abstract model verified in step 1. 

