/**
 * RBACSpecReader reads an RBAC spec file and returns an RBACInstance. 
 */
package mohawk.rbac;

import java.io.*;
import java.util.logging.Logger;

import org.antlr.v4.runtime.*;

import mohawk.global.parser.mohawk.v1.MohawkLexer;
import mohawk.global.parser.mohawk.v1.MohawkParser;

/** @author Karthick Jayaraman */
public class RBACSpecReader {
    public final static Logger logger = Logger.getLogger("mohawk");

    private RBACInstance rbac;

    public RBACSpecReader() {}

    public void RBACSpecReaderStr(String nusmvCode) throws IOException {
        InputStream is = new FileInputStream(nusmvCode);
        ANTLRInputStream input = new ANTLRInputStream(is);
        MohawkLexer lexer = new MohawkLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        MohawkParser parser = new MohawkParser(tokens);

        parser.initRbac();

        try {

            parser.init();
            rbac = new RBACInstance(parser.vRoles, parser.vUsers, parser.vAdmin, parser.mUA, parser.mCR, parser.mCA,
                    parser.vSpec);

        } catch (RecognitionException e) {
            e.printStackTrace();
        }
    }

    public RBACSpecReader(String filename) throws IOException {
        File rbacfile = new File(filename);

        if (!rbacfile.exists()) {
            System.out.println("The RBAC specification file " + rbacfile + " does not exists.");
            return;
        }

        InputStream is = new FileInputStream(filename);
        ANTLRInputStream input = new ANTLRInputStream(is);
        MohawkLexer lexer = new MohawkLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        MohawkParser parser = new MohawkParser(tokens);

        parser.initRbac();

        parser.init();
        rbac = new RBACInstance(parser.vRoles, parser.vUsers, parser.vAdmin, parser.mUA, parser.mCR, parser.mCA,
                parser.vSpec);
    }

    public RBACInstance getRBAC() {
        return rbac;
    }
}
