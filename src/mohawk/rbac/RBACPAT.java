/**
 *   RBACPAT - This tool converts an RBAC spec to an encoding used by the RBACPAT tool.
 */
package mohawk.rbac;

import java.io.*;
import java.util.*;

import org.antlr.v4.runtime.*;
import org.stringtemplate.v4.ST;

import mohawk.global.parser.mohawk.v1.MohawkLexer;
import mohawk.global.parser.mohawk.v1.MohawkParser;
import mohawk.global.pieces.mohawk.*;

/** @author Karthick Jayaraman
 * 
 *         PART OF CnvRBACPAT (STANDALONE) */
public class RBACPAT {

    private String filename;

    public RBACPAT(String in_fname) {
        filename = in_fname;
    }

    public StringBuffer getRBACPAT() throws Exception {

        File rbacfile = new File(filename);

        if (!rbacfile.exists()) {
            System.out.println("The RBAC specification file " + rbacfile + " does not exists.");
            return null;
        }

        RBACInstance rbac = getRBAC(filename);
        StringBuffer cablock = CanAssignBlock(rbac);
        StringBuffer crblock = CanRevokeBlock(rbac);

        StringBuffer rbacpat = new StringBuffer();
        rbacpat.append(cablock);
        rbacpat.append("\n");
        rbacpat.append(crblock);

        return rbacpat;
    }

    public RBACInstance getRBAC(String filename) throws IOException {
        InputStream is = new FileInputStream(filename);
        ANTLRInputStream input = new ANTLRInputStream(is);
        MohawkLexer lexer = new MohawkLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        MohawkParser parser = new MohawkParser(tokens);

        RBACInstance rbac = null;

        parser.initRbac();
        try {

            parser.init();
            rbac = new RBACInstance(parser.vRoles, parser.vUsers, parser.vAdmin, parser.mUA, parser.mCR, parser.mCA,
                    parser.vSpec);

        } catch (RecognitionException e) {
            e.printStackTrace();
        }

        return rbac;
    }

    public StringBuffer getPreCondition(RBACInstance inrbac, PreCondition prec) {

        StringBuffer strPreCnd = null;

        if (prec.size() == 0) {
            strPreCnd = new StringBuffer();
            strPreCnd.append("true");
            return strPreCnd;
        }

        for (int key : prec.keySet()) {
            String tmpRole = null;

            if (prec.getConditional(key) == 1) tmpRole = inrbac.getRoles().get(key);// "role"+key;
            else tmpRole = "-" + inrbac.getRoles().get(key);// "-role"+key

            if (strPreCnd == null) {
                strPreCnd = new StringBuffer();
                strPreCnd.append(tmpRole);
            } else {
                strPreCnd.append(" ");
                strPreCnd.append(tmpRole);
            }
        }
        return strPreCnd;
    }

    public StringBuffer CanAssignBlock(RBACInstance inrbac) {

        Map<String, Vector<CAEntry>> mCA = inrbac.getCA();
        StringBuffer canassignblock = null;

        for (String targetrole : mCA.keySet()) {
            Vector<CAEntry> vCA = mCA.get(targetrole);
            for (int i = 0; i < vCA.size(); i++) {
                ST canassign = new ST("can_assign($admin$,$conditions$,$target$)");
                PreCondition prec = vCA.get(i).getPreConditions();
                StringBuffer strPreCnd = getPreCondition(inrbac, prec);

                canassign.add("admin", vCA.get(i).getAdminRole());
                canassign.add("conditions", strPreCnd);
                canassign.add("target", targetrole);

                if (canassignblock == null) canassignblock = new StringBuffer();
                else canassignblock.append("\n");
                canassignblock.append(canassign);
            }
        }

        return canassignblock;
    }

    public StringBuffer CanRevokeBlock(RBACInstance inrbac) {

        Map<String, Vector<CREntry>> mCR = inrbac.getCR();
        StringBuffer canrevokeblock = null;
        Vector<String> vRoles = inrbac.getRoles();
        Set<String> sIRRevokableRoles = new HashSet<String>();

        /*
         * for(String targetrole: mCR.keySet()) { Vector<CREntry> vecCR = mCR.get(targetrole); Boolean found = false;
         * for(int i=0; i< vecCR.size(); i++) { CREntry cre = vecCR.get(i);
         * if(cre.getPreCond().equalsIgnoreCase("false")) found = true; } if(found) sIRRevokableRoles.add(targetrole); }
         */

        // for(int i=0; i<vRoles.size(); i++)
        // {
        // String strRole = vRoles.get(i);
        for (int i = 0; i < vRoles.size(); i++) {
            String strRole = vRoles.get(i);
            if (!mCR.keySet().contains(strRole)) sIRRevokableRoles.add(strRole);
        }

        for (String strRole : sIRRevokableRoles) {
            ST canrevoke = new ST("can_revoke($admin$,$target$)");
            canrevoke.add("admin", "false");
            canrevoke.add("target", strRole);

            if (canrevokeblock == null) canrevokeblock = new StringBuffer();
            else canrevokeblock.append("\n");
            canrevokeblock.append(canrevoke);
        }

        return canrevokeblock;
    }
}
