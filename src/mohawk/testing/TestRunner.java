package mohawk.testing;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.Callable;
import java.util.logging.Logger;

import mohawk.global.results.ExecutionResult;
import mohawk.global.results.TestingResult;
import mohawk.output.WriteRBACSpec;
import mohawk.refine.RolesAbsRefine;
import mohawk.singleton.MohawkSettings;

public class TestRunner implements Callable<TestingResult> {
    public final static Logger logger = Logger.getLogger("mohawk");

    public RolesAbsRefine absrefine = null;
    private MohawkSettings settings = MohawkSettings.getInstance();

    public TestRunner(RolesAbsRefine absrefine) {
        this.absrefine = absrefine;
    }

    @Override
    public TestingResult call() throws Exception {
        ExecutionResult result = null;

        System.out.println("Starting the test: " + new Date());

        if (settings.skipRefine) {
            try {
                WriteRBACSpec writer = new WriteRBACSpec();
                writer.Write2File(absrefine.getInputRBACInstance(), "logs/rbacinstancefile" + 1 + ".txt");
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            logger.info("[TestRunner] Skipping the Abstraction-Refinement Step");
            Boolean counterExampleFound = absrefine.getResult(absrefine.getInputRBACInstance(), 1);

            if (counterExampleFound) {
                result = ExecutionResult.GOAL_REACHABLE;
            } else if (absrefine.getReturnValue().equals("0")) {
                result = ExecutionResult.GOAL_UNREACHABLE;
            } else {
                result = ExecutionResult.ERROR_OCCURRED;
            }

        } else {
            logger.info("[TestRunner] Running the Abstraction-Refinement Step");
            result = absrefine.absrefineloop();
        }
        return new TestingResult(result, (long) 0, "", absrefine.getReturnValue(), "");
    }

}
