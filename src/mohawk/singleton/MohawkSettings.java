package mohawk.singleton;

import java.util.logging.Level;

import mohawk.global.results.MohawkResults;
import mohawk.global.results.TestingResult;
import mohawk.global.timing.MohawkTiming;
import mohawk.helper.SMVSpecHelper;
import mohawk.testing.TestingSuite;

public class MohawkSettings {
    private static volatile MohawkSettings _instance = null;

    public String Logger_filepath = "mohawk.csv";
    public String Logger_folderpath = "logs";
    public Level LoggerLevel;
    public Boolean WriteCSVFileHeader = true;

    public String timingResultsFile = "logs/mohawkTimingResults.csv";
    public String testingResultsFile = "logs/mohawkTestingResults.csv";
    public String smvFilepath = "latestRBAC2SMV.smv";

    // Reference
    protected String NuSMV_filepath = "NuSMV";

    public String getNuSMV_filepath() {
        return NuSMV_filepath;
    }

    public void setNuSMV_filepath(String nuSMV_filepath) {
        NuSMV_filepath = nuSMV_filepath;
    }

    // Helpers
    public MohawkResults results;
    public MohawkTiming timing;
    public TestingSuite tests;
    public SMVSpecHelper smvHelper;
    public TestingResult lastResult;

    // SMV
    public Long TIMEOUT_SECONDS = (long) 0;// Default 0 (infinite)
    public boolean bulk = false;
    public Boolean smvDeleteFile = false;

    public String specFile = "";
    public String specFileExt = ".mohawk";

    public int mode = 3;// 1 for bmc, 2 for smc, 3 for both

    /** Flag for Slicing the RBAC Specification file before entering the Refinement Loop */
    public boolean sliceRBAC = true;

    /** Flag for Slicing the RBAC Specification Query before entering the Refinement Loop */
    public boolean sliceRBACQuery = false;

    /** Flag for skipping the abstraction-refinement step */
    public boolean skipRefine = false;

    /** Flag for optimizing RBAC instances by removing the roles that are not present in any rules as a target
     * and replacing them in all CA rule conditions as a constant (TRUE or FALSE) */
    public boolean removeUnsetRolesRBAC = true;

    /** Flag that will run heuristics that speeds up results for easy/simple test cases */
    public boolean runHeuristics = true;;

    protected MohawkSettings() {}

    public static MohawkSettings getInstance() {
        if (_instance == null) {
            _instance = new MohawkSettings();
        }
        return _instance;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();

        s.append("MohawkSettings{");
        s.append("sliceRBAC: ").append(sliceRBAC).append("; ");
        s.append("sliceRBACQuery: ").append(sliceRBACQuery).append("; ");
        s.append("skipRefine: ").append(skipRefine).append("; ");
        s.append("removeUnsetRolesRBAC: ").append(removeUnsetRolesRBAC).append("; ");
        s.append("NuSMV_filepath: ").append(NuSMV_filepath);
        s.append("}");

        return s.toString();
    }
}
