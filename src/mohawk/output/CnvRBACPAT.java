/**
 * The MIT License
 * 
 * Copyright (c) 2010 Karthick Jayaraman
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */

/**
 *  This class converts from a plain RBAC spec to RBACPAT format
 */
package mohawk.output;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import mohawk.rbac.RBACPAT;

/** @author Karthick Jayaraman
 * 
 *         STAND ALONE */
public class CnvRBACPAT {

    public static void main(String args[]) throws IOException, Exception {

        if (args.length < 2) {
            System.out.println("Usage : java -jar mohawk.jar CnvRBACPAT sourcefile destfile");
            return;
        }

        File rbacfile = new File(args[0]);

        if (!rbacfile.exists()) {
            System.out.println("The RBAC specification file " + rbacfile + " does not exists.");
            return;
        }

        String nusmvfile = args[1];

        RBACPAT rbacpat = new RBACPAT(args[0]);
        writeFile(nusmvfile, rbacpat.getRBACPAT());

        return;
    }

    static void writeFile(String filename, StringBuffer contents) throws IOException {

        FileWriter file = new FileWriter(filename);
        file.write(contents.toString());
        file.close();
    }

}
