/**
 * 			RBAC2SMV - Tool for converting a RBAC specification
 *                     to NuSMV specification
 *                     
 *                     Author: Karthick Jayaraman
 */
package mohawk.output;

import java.io.*;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;

import org.stringtemplate.v4.ST;

import mohawk.global.pieces.mohawk.*;
import mohawk.global.timing.MohawkTiming;
import mohawk.rbac.RBACInstance;

/*
 * This class is in charge of writing the NuSMV specification to a file using 
 * StringTemplate
 */

public class WriteNuSMV {

    /** Filename on disk that will contain the NuSMV spec */
    private String filename;

    /** Flag indicates whether the SMV specification has been initialized in the string. */
    private Boolean done;

    /** SMV spec code */
    private String smvcode;

    /** Flag that indicates if you want to old format, pre 2.5.0 */
    public Boolean olderVersion = false;

    // RBAC Instance
    private RBACInstance rbac;

    /** This setting changes how the user transitions are computed
     * The result should be functionally the same, but organized differently
     * WARNING: version 2 DOES NOT WORK and is incomplete, it must be finished up later */
    public Integer userTransistionVersion = 1;

    private ST strTCodeTemplateV1;
    private ST strTCodeTemplateV2;

    private String strTransTemplateV1;
    // private String strTransTemplateV2;

    /** Controls the output for this class */
    public Level logLevel = Level.WARNING;

    public boolean includeRuleComment = true;
    public Integer roleWithRules;
    public Integer roleWithoutRules;

    // Timing
    public MohawkTiming timing;
    public String timingPrefix;

    /*
     * The constructor should initialize the target file and the filename for the template.
     */
    public WriteNuSMV(String fname, MohawkTiming timing, String timingPrefix) {
        this.timing = timing;
        this.timingPrefix = timingPrefix;

        filename = fname;
        done = false;
        roleWithRules = 0;
        roleWithoutRules = 0;

        try {
            // String template = ConvertTo.readFile(this.getClass().getResource("smvtemplate.st"));
            strTCodeTemplateV1 = new ST(getTemplateStr("smvtemplatev1.st"), '$', '$');
            strTransTemplateV1 = getTemplateStr("transitionsv1.st");

            strTCodeTemplateV2 = new ST(getTemplateStr("smvtemplatev2.st"), '<', '>');
            // strTransTemplateV2 = getTemplateStr("transitionsv2.st");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getTrue() {
        return ((olderVersion) ? "1" : "TRUE");
    }

    public String getFalse() {
        return ((olderVersion) ? "0" : "FALSE");
    }

    public Vector<CAEntry> getMatchingCA(String strRole) {

        /*
         * Vector<CAEntry> vFullCA = rbac.getCA(); Vector<CAEntry> vFilCA = new Vector<CAEntry>();
         * 
         * for(int i=0; i<vFullCA.size(); i++) { CAEntry tmpCA = vFullCA.get(i); if(tmpCA.strRole.equals(strRole))
         * vFilCA.add(tmpCA); }
         */

        Map<String, Vector<CAEntry>> mCA = rbac.getCA();

        if (mCA != null) return mCA.get(strRole);
        else return null;

        // return rbac.getCA().get(strRole);
        // return vFilCA;

    }

    public Vector<CREntry> getMatchingCR(String strRole) {

        /*
         * Vector<CREntry> vFullCR = rbac.getCR(); Vector<CREntry> vFilCR = new Vector<CREntry>();
         * 
         * for(int i=0; i<vFullCR.size(); i++) { CREntry tmpCR = vFullCR.get(i); if(tmpCR.strRole.equals(strRole))
         * vFilCR.add(tmpCR); }
         * 
         * return vFilCR;
         */
        return rbac.getCR().get(strRole);
    }

    /* Load template contents from textfile */
    public String getTemplateStr(String filename) throws IOException {

        InputStream is = getClass().getResourceAsStream(filename);
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String strtemplate = null;
        String line;
        while ((line = br.readLine()) != null) {
            if (strtemplate == null) strtemplate = line;
            else strtemplate = strtemplate + "\n" + line;
        }
        // System.out.println(strtemplate);

        return strtemplate;
    }

    public String getNuSMVCode() {
        return this.smvcode;
    }

    /*
     * Fill attributes in the StringTemplate
     * 
     * Maybe This function should be passed an object that represents the RBAC state. The parameters should be plugged
     * into the template based on the RBAC state.
     */
    public void fillAttributes(RBACInstance inRbac) throws Exception {
        /* TIMING */timing.startTimer(timingPrefix + " (fillAttributes)");
        rbac = inRbac;

        // StringTemplate StrTSmvspec = new StringTemplate(strtemplate);
        // StringTemplate strTrans = new StringTemplate(transtemplate);

        System.out.println("[START] WriteNuSMV.fillAttributes()");

        System.out.println("[START] Setting up user arrays...");
        /* TIMING */timing.startTimer(timingPrefix + " (Setting up user arrays)");
        setupUsers(strTCodeTemplateV1);
        /* TIMING */timing.stopTimer(timingPrefix + " (Setting up user arrays)");

        System.out.println("[START] Setting up UA...");
        /* TIMING */timing.startTimer(timingPrefix + " (Setting up UA)");
        setupUA(strTCodeTemplateV1);
        /* TIMING */timing.stopTimer(timingPrefix + " (Setting up UA)");

        System.out.println("[START] Setting up CA-CR rules (Using version " + userTransistionVersion + ") ...");
        /* TIMING */timing.startTimer(timingPrefix + " (Setting up CA-CR rules v" + userTransistionVersion + ")");
        switch (userTransistionVersion) {
            case 1 :
                setupUserTransitionsv1(strTCodeTemplateV1);
                break;
            case 2 :
                setupUserTransitionsv2(strTCodeTemplateV2);
                break;
        }
        /* TIMING */timing.stopTimer(timingPrefix + " (Setting up CA-CR rules v" + userTransistionVersion + ")");

        // Setup LTLSPEC
        System.out.println("[START] Setting up SPEC...");
        /* TIMING */timing.startTimer(timingPrefix + " (Setting up SPEC)");
        String strUser = rbac.getSpec().get(0);
        String strRole = rbac.getSpec().get(1);
        setupSpec(strTCodeTemplateV1, strUser, strRole);
        /* TIMING */timing.stopTimer(timingPrefix + " (Setting up SPEC)");
        // this.setupTransactions(this.strCodeTemplate, this.strCodeTemplate);

        System.out.println("[START] Rendering SMV Code...");
        /* TIMING */timing.startTimer(timingPrefix + " (Rendering SMV Code)");
        smvcode = strTCodeTemplateV1.render();
        /* TIMING */timing.stopTimer(timingPrefix + " (Rendering SMV Code)");

        done = true;

        /* TIMING */timing.stopTimer(timingPrefix + " (fillAttributes)");
    }

    public void setupUsers(ST inSmvSpec) {

        int noRoles = rbac.getNumRoles();
        Vector<String> vUsers = rbac.getUsers();
        Vector<String> vAdmin = rbac.getAdmin();

        for (int i = 0; i < vUsers.size(); i++) {
            String user = vUsers.get(i);
            inSmvSpec.add("userarrays", user + " : array 0.." + (noRoles - 1) + " of boolean");
            if (olderVersion) {
                inSmvSpec.add("users", user);
            } else {
                inSmvSpec.add("users", shortenUser(user));
            }
        }

        for (int i = 0; i < vAdmin.size(); i++) {
            String user = vAdmin.get(i);
            if (olderVersion) {
                inSmvSpec.add("admin", user);
            } else {
                inSmvSpec.add("admin", shortenUser(user));
            }
        }

        inSmvSpec.add("role", "0 .. " + (noRoles - 1));

    }

    public String shortenUser(String user) {
        if (user.startsWith("user")) {
            return user.replace("user", "u");
        } else {
            return user + "_" + user.length();
        }
    }

    public void setupUA(ST inSmvSpec) {

        Vector<String> vUsers = rbac.getUsers();
        Vector<String> vRoles = rbac.getRoles();
        // Set<String> setUsers = rbac.getUA().keySet();
        int no_Roles = vRoles.size();
        Map<String, Vector<Integer>> mUA = rbac.getUA();

        for (int i = 0; i < vUsers.size(); i++) {
            String user = vUsers.get(i);
            for (int j = 0; j < no_Roles; j++) {
                Vector<Integer> vUserUA = mUA.get(user);

                String value;

                if (vUserUA != null) {
                    if (vUserUA.contains(j)) {
                        value = getTrue();
                    } else {
                        value = getFalse();
                    }
                } else {
                    value = getFalse();
                }

                String uaassign = "init(" + user + "[" + j + "]" + ") := " + value;
                inSmvSpec.add("ua", uaassign);
            }
            // mUA.remove(user);
        }
    }

    public void setupUserTransitionsv2(ST strTCodeTemplate) throws Exception {
        Vector<String> vUsers = rbac.getUsers();

        String strTransitions = this.setupEachUserv2(vUsers);

        strTCodeTemplate.add("transitions", strTransitions);
    }

    public String setupEachUserv2(Vector<String> vUsers) throws Exception {
        Vector<String> vRoles = rbac.getRoles();
        String strUserSection = null;

        for (int i = 0; i < vRoles.size(); i++) {
            String strRole = vRoles.get(i);
            Vector<CAEntry> vCA = getMatchingCA(strRole);
            Vector<CREntry> vCR = getMatchingCR(strRole);
            StringBuilder transitions = null;
            String strTmpUser = null;
            ST strTTrans = new ST(strTransTemplateV1, '$', '$');

            if (vCA != null) {
                for (int j = 0; j < vCA.size(); j++) {
                    String strTmpTrans = addCAEntryv2(vUsers, vCA.get(j));
                    if (transitions == null) {
                        transitions = new StringBuilder();
                        transitions = transitions.append(strTmpTrans);
                    } else {
                        transitions.append("\n");
                        transitions.append(strTmpTrans);
                        // transitions = transitions + "\n" + strTmpTrans;
                    }
                }
            }

            if (vCR != null) {
                for (int j = 0; j < vCR.size(); j++) {
                    String strTmpTrans = addCREntryv2(vUsers, vCR.get(j));
                    if (transitions == null) {
                        transitions = new StringBuilder();
                        transitions = transitions.append(strTmpTrans);
                    } else {
                        transitions.append("\n");
                        transitions.append(strTmpTrans);
                        // transitions = transitions + "\n" + strTmpTrans;
                    }
                }
            }

            if (transitions != null) {
                strTTrans.add("rolename", strRole);
                strTTrans.add("transition", transitions);
                strTTrans.add("trueWord", getTrue());
                strTmpUser = "";
                for (String u : vUsers) {
                    strTTrans.add("roleindex1", u + "[" + i + "]");
                    strTTrans.add("roleindex2", u + "[" + i + "]");
                    strTmpUser += strTTrans.render() + "\n";
                }
                roleWithRules++;
            } else {
                strTmpUser = "";
                for (String u : vUsers) {
                    strTmpUser += "-- " + strRole + "\nnext(" + u + "[" + i + "]) := " + u + "[" + i + "];\n";
                }
                roleWithoutRules++;
            }

            if (strUserSection == null) {
                strUserSection = strTmpUser;
            } else {
                strUserSection = strUserSection + "\n\n" + strTmpUser;
            }
        }

        return strUserSection;
    }

    public String addCAEntryv2(Vector<String> vUsers, CAEntry inCAEntry) throws Exception {
        Vector<String> vAdminUsers = rbac.getAdmin();
        PreCondition pcPreCond = inCAEntry.getPreConditions();
        String strTransition = null;
        String strPreCond = null;
        String strCond = null;
        int iDestRoleIndex = 0;
        int adminRoleIndex = 0;

        iDestRoleIndex = rbac.getRoleIndex(inCAEntry.getRole());
        adminRoleIndex = rbac.getRoleIndex(inCAEntry.getAdminRole());
        if (pcPreCond.size() != 0) {
            for (int iroleindex : pcPreCond.keySet()) {
                int value = pcPreCond.getConditional(iroleindex);

                if (value == 1) {
                    strCond = "$user$[" + iroleindex + "]=$True$";
                } else {
                    strCond = "$user$[" + iroleindex + "]=$False$";
                }

                if (strPreCond == null) {
                    strPreCond = strCond;
                } else {
                    strPreCond = strPreCond + " & " + strCond;
                }
            }
        } else {
            strPreCond = getTrue();
        }

        String strTemp = "user = $user$ & " + "admin = $admin$ & " + "$admin$[$adminRoleIndex$] = $True$ & "
                + strPreCond + " & " + "role = " + iDestRoleIndex + " & " + "act = ADD: " + getTrue() + ";";

        for (String vAdminUser : vAdminUsers) {
            for (String vUser : vUsers) {
                ST strTTrans = new ST(strTemp, '$', '$');

                strTTrans.add("user", shortenUser(vUser));
                strTTrans.add("admin", shortenUser(vAdminUser));
                strTTrans.add("True", getTrue());
                strTTrans.add("False", getFalse());
                strTTrans.add("adminRoleIndex", adminRoleIndex);

                if (strTransition == null) {
                    strTransition = strTTrans.render();
                } else {
                    strTransition = strTransition + "\n" + strTTrans.render();
                }
            }
        }

        if (includeRuleComment) {
            strTransition = "-- CA Rule: " + inCAEntry + "\n" + strTransition;
        }

        return strTransition;
    }

    public String addCREntryv2(Vector<String> vUsers, CREntry crentry) {

        int adminRoleIndex = 0;
        Vector<String> vAdminUsers = rbac.getAdmin();
        String strTransition = null;
        int iDestRoleIndex = 0;

        try {
            iDestRoleIndex = rbac.getRoleIndex(crentry.getTargetRole());

            if (crentry.getAdminRole().equalsIgnoreCase("true")) {
                adminRoleIndex = -1;
            } else if (crentry.getAdminRole().equalsIgnoreCase("false")) {
                adminRoleIndex = -2;
            } else {
                adminRoleIndex = rbac.getRoleIndex(crentry.getAdminRole());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        String strTemp;

        strTemp = "user = $user$ & admin = $admin$ & $precondition$ & role = " + iDestRoleIndex + " & act = REMOVE: "
                + getFalse() + ";";

        for (String vAdminUser : vAdminUsers) {
            for (String vUser : vUsers) {
                ST strTTrans = new ST(strTemp, '$', '$');

                strTTrans.add("user", shortenUser(vUser));
                strTTrans.add("admin", shortenUser(vAdminUser));

                if (adminRoleIndex == -1) strTTrans.add("precondition", getTrue());
                else if (adminRoleIndex == -2) strTTrans.add("precondition", getFalse());
                else strTTrans.add("precondition", vAdminUser + "[" + adminRoleIndex + "]=" + getTrue());

                if (strTransition == null) {
                    strTransition = strTTrans.render();
                } else {
                    strTransition = strTransition + "\n" + strTTrans.render();
                }
            }
        }

        if (includeRuleComment) {
            strTransition = "-- CR Rule: " + crentry + "\n" + strTransition;
        }

        return strTransition;
    }

    // OLD Versions
    public void setupUserTransitionsv1(ST strTCodeTemplate) {
        Vector<String> vUsers = rbac.getUsers();

        for (int i = 0; i < vUsers.size(); i++) {
            if (logLevel.intValue() <= Level.FINE.intValue()) {
                System.out.println("Writing for user " + vUsers.get(i));
            }
            String strUser = vUsers.get(i);
            String strTransitions = this.setupEachUserv1(strUser);

            strTCodeTemplate.add("transitions", strTransitions);
        }
    }

    public String setupEachUserv1(String inUser) {

        Vector<String> vRoles = rbac.getRoles();
        String strUserSection = null;

        for (int i = 0; i < vRoles.size(); i++) {
            String strRole = vRoles.get(i);
            Vector<CAEntry> vCA = getMatchingCA(strRole);
            Vector<CREntry> vCR = getMatchingCR(strRole);
            StringBuilder transitions = null;
            String strTmpUser = null;
            ST strTTrans = new ST(strTransTemplateV1, '$', '$');

            if (vCA != null) {
                for (int j = 0; j < vCA.size(); j++) {
                    String strTmpTrans = addCAEntryv1(inUser, vCA.get(j));
                    if (transitions == null) {
                        transitions = new StringBuilder();
                        transitions = transitions.append(strTmpTrans);
                    } else {
                        transitions.append("\n");
                        transitions.append(strTmpTrans);
                        // transitions = transitions + "\n" + strTmpTrans;
                    }
                }
            }

            if (vCR != null) {
                for (int j = 0; j < vCR.size(); j++) {
                    String strTmpTrans = addCREntryv1(inUser, vCR.get(j));
                    if (transitions == null) {
                        transitions = new StringBuilder();
                        transitions = transitions.append(strTmpTrans);
                    } else {
                        transitions.append("\n");
                        transitions.append(strTmpTrans);
                        // transitions = transitions + "\n" + strTmpTrans;
                    }
                }
            }

            if (transitions != null) {
                strTTrans.add("rolename", strRole);
                strTTrans.add("transition", transitions);
                strTTrans.add("trueWord", getTrue());
                strTTrans.add("roleindex1", inUser + "[" + i + "]");
                strTTrans.add("roleindex2", inUser + "[" + i + "]");
                strTmpUser = strTTrans.render();
                roleWithRules++;
            } else {
                strTmpUser = "-- " + strRole + "\nnext(" + inUser + "[" + i + "]) := " + inUser + "[" + i + "];";
                roleWithoutRules++;
            }

            if (strUserSection == null) {
                strUserSection = strTmpUser;
            } else {
                strUserSection = strUserSection + "\n\n" + strTmpUser;
            }
        }

        return strUserSection;
    }

    public String addCAEntryv1(String inUser, CAEntry inCAEntry) {

        Vector<String> vAdminUsers = rbac.getAdmin();
        String strTransition = null;
        int iDestRoleIndex = 0;
        try {
            iDestRoleIndex = rbac.getRoleIndex(inCAEntry.getRole());
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        String strTemp = "user = $user$ & admin = $admin$ & $admincond$ & $precondition$ & role = " + iDestRoleIndex
                + " & act = ADD: " + getTrue() + ";";

        for (int i = 0; i < vAdminUsers.size(); i++) {
            ST strTTrans = new ST(strTemp, '$', '$');

            if (olderVersion) {
                strTTrans.add("user", inUser);
                strTTrans.add("admin", vAdminUsers.get(i));
            } else {
                strTTrans.add("user", shortenUser(inUser));
                strTTrans.add("admin", shortenUser(vAdminUsers.get(i)));
            }
            PreCondition pcPreCond = inCAEntry.getPreConditions();
            String strPreCond = null;
            int adminRoleIndex = 0;
            try {
                adminRoleIndex = rbac.getRoleIndex(inCAEntry.getAdminRole());
            } catch (Exception e) {
                System.out.println(inCAEntry.getAdminRole());
                e.printStackTrace();
            }

            String strAdminCondRole = vAdminUsers.get(i) + "[" + adminRoleIndex + "] = " + getTrue();

            strTTrans.add("admincond", strAdminCondRole);

            if (pcPreCond.size() != 0) {
                for (int iroleindex : pcPreCond.keySet()) {
                    int value = pcPreCond.getConditional(iroleindex);

                    String strCond = null;

                    if (value == 1) {
                        strCond = inUser + "[" + iroleindex + "]=" + getTrue();
                    } else {
                        strCond = inUser + "[" + iroleindex + "]=" + getFalse();
                    }

                    if (strPreCond == null) {
                        strPreCond = strCond;
                    } else {
                        strPreCond = strPreCond + " & " + strCond;
                    }
                }
            } else {
                strPreCond = getTrue();
            }

            strTTrans.add("precondition", strPreCond);

            if (strTransition == null) strTransition = strTTrans.render();
            else strTransition = strTransition + "\n" + strTTrans.render();
        }

        if (includeRuleComment) {
            strTransition = "-- CA Rule: " + inCAEntry + "\n" + strTransition;
        }

        return strTransition;
    }

    public String addCREntryv1(String inUser, CREntry crentry) {

        int adminRoleIndex = 0;
        Vector<String> vAdminUsers = rbac.getAdmin();
        String strTransition = null;
        int iDestRoleIndex = 0;

        try {
            iDestRoleIndex = rbac.getRoleIndex(crentry.getTargetRole());

            if (crentry.getAdminRole().equalsIgnoreCase("true")) {
                adminRoleIndex = -1;
            } else if (crentry.getAdminRole().equalsIgnoreCase("false")) {
                adminRoleIndex = -2;
            } else {
                adminRoleIndex = rbac.getRoleIndex(crentry.getAdminRole());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        String strTemp;

        strTemp = "user = $user$ & admin = $admin$ & $precondition$ & role = " + iDestRoleIndex + " & act = REMOVE: "
                + getFalse() + ";";

        for (int i = 0; i < vAdminUsers.size(); i++) {
            ST strTTrans = new ST(strTemp, '$', '$');

            if (olderVersion) {
                strTTrans.add("user", inUser);
                strTTrans.add("admin", vAdminUsers.get(i));
            } else {
                strTTrans.add("user", shortenUser(inUser));
                strTTrans.add("admin", shortenUser(vAdminUsers.get(i)));
            }

            if (adminRoleIndex == -1) strTTrans.add("precondition", getTrue());
            else if (adminRoleIndex == -2) strTTrans.add("precondition", getFalse());
            else strTTrans.add("precondition", vAdminUsers.get(i) + "[" + adminRoleIndex + "]=" + getTrue());

            if (strTransition == null) strTransition = strTTrans.render();
            else strTransition = strTransition + "\n" + strTTrans.render();
        }

        if (includeRuleComment) {
            strTransition = "-- CR Rule: " + crentry + "\n" + strTransition;
        }

        return strTransition;
    }

    public void setupSpec(ST strTCodeTemplate, String inStrUser, String inStrRole) {

        int roleindex = 0;
        try {
            roleindex = rbac.getRoleIndex(inStrRole);
        } catch (Exception e) {
            e.printStackTrace();
        }

        strTCodeTemplate.add("user", inStrUser);
        strTCodeTemplate.add("roleindex", roleindex);
        strTCodeTemplate.add("falseWord", getFalse());
        strTCodeTemplate.add("roleWithoutRules", roleWithoutRules);
        strTCodeTemplate.add("roleWithRules", roleWithRules);
        strTCodeTemplate.add("roleWithoutWithRatio", new Float(roleWithoutRules) / roleWithRules);
        strTCodeTemplate.add("roleWithoutTotalRatio", new Float(roleWithoutRules) / (roleWithoutRules + roleWithRules));

    }

    public boolean writeFile() throws IOException {
        if (done) {
            FileWriter file = new FileWriter(filename);
            file.write(smvcode);
            file.close();
            return true;
        }
        return false;
    }

    public String getFilename() {
        return filename;
    }

}
