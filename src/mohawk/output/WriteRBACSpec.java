/**
 *  Tool for generating RBAC policy specification. 
 */
package mohawk.output;

import java.io.*;
import java.util.Map;
import java.util.Vector;

import org.stringtemplate.v4.ST;

import mohawk.global.pieces.mohawk.*;
import mohawk.rbac.RBACInstance;

/** @author Karthick Jayaraman This class is for writing an a generated RBAC instance to file. */
public class WriteRBACSpec {

    // StringTemplate for RBAC instance
    private ST STRBACSpec;
    private String templateFilename = "rbac.st";

    // RBAC instance to write to file.
    private RBACInstance rbacinstance;

    public WriteRBACSpec() {
        try {
            String strTemplateString = this.getTemplateStr(templateFilename);
            STRBACSpec = new ST(strTemplateString, '$', '$');
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getTemplateStr(String filename) throws IOException {

        InputStream is = getClass().getResourceAsStream(filename);
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String strtemplate = null;
        String line;
        while ((line = br.readLine()) != null) {
            if (strtemplate == null) strtemplate = line;
            else strtemplate = strtemplate + "\n" + line;
        }

        return strtemplate;
    }

    private void writeRoles() {

        Vector<String> vecRoles = rbacinstance.getRoles();

        for (int i = 0; i < vecRoles.size(); i++) {
            String strRole = vecRoles.get(i);
            this.STRBACSpec.add("roles", strRole);
        }
    }

    public void writeUsers() {
        Vector<String> vecNUsers = rbacinstance.getUsers();
        Vector<String> vecAUsers = rbacinstance.getAdmin();

        for (int i = 0; i < vecNUsers.size(); i++)
            this.STRBACSpec.add("users", vecNUsers.get(i));

        for (int i = 0; i < vecAUsers.size(); i++)
            this.STRBACSpec.add("adminusers", vecAUsers.get(i));
    }

    public void writeUA() {

        Map<String, Vector<Integer>> mUA = rbacinstance.getUA();

        for (String user : mUA.keySet()) {
            Vector<Integer> vecUserUA = mUA.get(user);

            for (int i = 0; i < vecUserUA.size(); i++) {
                ST STUA = new ST("<$userroles; separator=\",\"$>", '$', '$');
                String role = rbacinstance.getRoles().get(vecUserUA.get(i));
                STUA.add("userroles", user);
                STUA.add("userroles", role);
                this.STRBACSpec.add("ua", STUA.render());
            }
        }

    }

    // write CR to the file.
    public void writeCR() {
        boolean setOne = false;
        Map<String, Vector<CREntry>> mCR = rbacinstance.getCR();

        // When the RBACInstance is created, it does not create CR rules of the
        // form <FALSE,role> We choose first two CR entries
        // and convert them to TRUE / FALSE entries randomly.
        for (String targetrole : mCR.keySet()) {
            Vector<CREntry> vecCR = mCR.get(targetrole);
            for (int i = 0; i < vecCR.size(); i++) {
                CREntry cre = vecCR.get(i);
                ST STCR = new ST("<$crrule; separator=\",\"$>", '$', '$');

                if (i < 2) {
                    // if(rndGen.nextInt(2) == 1)
                    // vecCR.get(i).setPreCond("FALSE");
                    STCR.add("crrule", cre.getAdminRole());
                }

                STCR.add("crrule", cre.getTargetRole());
                STRBACSpec.add("cr", STCR.render());
                setOne = true;
            }
        }

        if (setOne == false) {
            // Removes error: "context [anonymous] 7:4 attribute cr isn't defined"
            STRBACSpec.add("cr", "");
        }
    }

    // write CA to the file.
    public void writeCA() {
        boolean setOne = false;
        Map<String, Vector<CAEntry>> mCA = rbacinstance.getCA();

        for (String targetrole : mCA.keySet()) {
            Vector<CAEntry> vCA = mCA.get(targetrole);

            for (int i = 0; i < vCA.size(); i++) {
                ST STCR = new ST("<$carule; separator=\",\"$>", '$', '$');
                CAEntry cae = vCA.get(i);
                String strAdminRole = cae.getAdminRole();
                PreCondition PreCnd = cae.getPreConditions();
                String strRole = cae.getRole();

                STCR.add("carule", strAdminRole);
                StringBuffer strPreCnd = null;

                if (PreCnd.size() == 0) {
                    strPreCnd = new StringBuffer();
                    strPreCnd.append("TRUE"); // If there are no conditionals in
                    // the PreCondition, set the
                    // precondition to TRUE
                } else {
                    for (int key : PreCnd.keySet()) {
                        String tmpRole;

                        if (PreCnd.getConditional(key) == 1) tmpRole = rbacinstance.getRoles().get(key);// "role"+key;
                        else tmpRole = "-" + rbacinstance.getRoles().get(key);// "-role"+key;

                        if (strPreCnd == null) {
                            strPreCnd = new StringBuffer();
                            strPreCnd.append(tmpRole);
                        } else {
                            strPreCnd.append("&");
                            strPreCnd.append(tmpRole);
                        }
                    }
                }

                STCR.add("carule", strPreCnd.toString());
                STCR.add("carule", strRole);
                this.STRBACSpec.add("ca", STCR.render());
                setOne = true;
            }
        }
        if (setOne == false) {
            // Removes error: "context [anonymous] 7:4 attribute ca isn't defined"
            STRBACSpec.add("ca", "");
        }
    }

    public void writeSpec() {
        Vector<String> vSpec = rbacinstance.getSpec();
        STRBACSpec.add("spec", vSpec.get(0));
        STRBACSpec.add("spec", vSpec.get(1));
    }

    public void Write2File(RBACInstance inrbacinstance, String filename) throws IOException {

        FileWriter file = new FileWriter(filename);
        rbacinstance = inrbacinstance;

        System.out.println("Writing roles");
        writeRoles();
        System.out.println("Writing users");
        writeUsers();
        System.out.println("Writing UA");
        writeUA();
        System.out.println("Writing CR");
        writeCR();
        System.out.println("Writing CA");
        writeCA();
        System.out.println("Writing SPEC");
        writeSpec();
        String filestring = STRBACSpec.render();
        file.write(filestring);
        file.close();

    }

}
