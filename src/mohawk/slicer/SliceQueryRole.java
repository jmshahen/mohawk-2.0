/**
 * 
 */
package mohawk.slicer;

import java.util.*;
import java.util.logging.Logger;

import mohawk.global.pieces.mohawk.CAEntry;
import mohawk.global.pieces.mohawk.CREntry;
import mohawk.rbac.RBACInstance;

/** @author kjayaram */
public class SliceQueryRole {
    public final static Logger logger = Logger.getLogger("mohawk");

    private RBACInstance unsliced;
    // private RBACInstance sliced; // NEVER USED
    private String strRole;
    // private String strUser; // NEVER USED

    private Vector<String> slicedRoles;
    private Vector<String> slicedUsers;
    private Vector<String> slicedAdmin;
    private Map<String, Vector<Integer>> slicedUA;
    private Map<String, Vector<CREntry>> slicedCR;
    private Map<String, Vector<CAEntry>> slicedCA;
    // private Vector<String> slicedSpec; // NEVER USED

    // Helpers for identifying dependencies
    // private Vector<String> vPositiveDeps; // NEVER USED
    // private Vector<String> vNegativeDeps; // NEVER USED

    /*
     * This is a map between role indices of unsliced roles and and sliced roles.
     */
    // private Map<Integer, Integer> mapRoleIndex; // NEVER USED

    public SliceQueryRole(RBACInstance inRbac) {
        unsliced = inRbac;
        strRole = unsliced.getSpec().get(1);
        // strUser = unsliced.getSpec().get(0); // NEVER USED
        // mapRoleIndex = new HashMap<Integer, Integer>(); // NEVER USED
    }

    public RBACInstance getSlicedPolicy() throws Exception {

        // No changes to roles, users, admin, UA, and CR.
        slicedRoles = unsliced.getRoles();
        slicedUsers = unsliced.getUsers();
        slicedAdmin = unsliced.getAdmin();
        slicedUA = unsliced.getUA();
        slicedCR = unsliced.getCR();

        System.out.println("Slicing CA");
        slicedCA = sliceCA();

        return new RBACInstance(slicedRoles, slicedUsers, slicedAdmin, slicedUA, slicedCR, slicedCA,
                unsliced.getSpec());
    }

    /*
     * This function is incharge of slicing the CA rules.
     */
    Map<String, Vector<CAEntry>> sliceCA() throws Exception {

        Map<String, Vector<CAEntry>> newCA = new HashMap<String, Vector<CAEntry>>();
        int iQueryRoleIndex = unsliced.getRoleIndex(this.strRole);

        for (String strRole : unsliced.getCA().keySet()) {
            Vector<CAEntry> vCAE = unsliced.getCA().get(strRole);
            Vector<CAEntry> vNewCAE = new Vector<CAEntry>();

            Iterator<CAEntry> itr = vCAE.iterator();
            while (itr.hasNext()) {
                CAEntry cae = itr.next();

                if (!cae.getPreConditions().keySet().contains(iQueryRoleIndex)) vNewCAE.add(cae);
                else {
                    if (cae.getPreConditions().getConditional(iQueryRoleIndex) == 2) {
                        cae.getPreConditions().delConditional(iQueryRoleIndex);
                        vNewCAE.add(cae);
                    }
                }
            }

            if (vNewCAE.size() != 0) newCA.put(strRole, vNewCAE);
        }

        return newCA;
    }

}
