/**
 * 
 */
package mohawk.slicer;

import java.util.*;
import java.util.logging.Logger;

import mohawk.global.pieces.mohawk.*;
import mohawk.global.timing.MohawkTiming;
import mohawk.rbac.RBACInstance;

/** @author kjayaram */
public class RoleSlicer {
    public final static Logger logger = Logger.getLogger("mohawk");

    private RBACInstance unsliced;
    // private RBACInstance sliced; // NEVER USED
    private String strRole;
    private String strUser;

    private Vector<String> slicedRoles;
    private Vector<String> slicedUsers;
    private Vector<String> slicedAdmin;
    private Map<String, Vector<Integer>> slicedUA;
    private Map<String, Vector<CREntry>> slicedCR;
    private Map<String, Vector<CAEntry>> slicedCA;
    // private Vector<String> slicedSpec; // NEVER USED

    // Helpers for identifying dependencies
    private Vector<String> vPositiveDeps;
    private Vector<String> vNegativeDeps;

    // Timing
    public MohawkTiming timing;
    public String timingPrefix;

    /*
     * This is a map between role indices of unsliced roles and and sliced roles.
     */
    private Map<Integer, Integer> mapRoleIndex;

    public RoleSlicer(RBACInstance inRbac, MohawkTiming timing, String timingPrefix) {
        this.timing = timing;
        this.timingPrefix = timingPrefix;
        unsliced = inRbac;
        strRole = unsliced.getSpec().get(1);
        strUser = unsliced.getSpec().get(0);
        mapRoleIndex = new HashMap<Integer, Integer>();

    }

    public RBACInstance getSlicedPolicy() throws Exception {
        /* TIMING */timing.startTimer(timingPrefix + " (slicingTotal)");

        System.out.println("[START] RoleSlicer.getSlicedPolicy()");

        System.out.println("[START] Slicing roles");
        /* TIMING */timing.startTimer(timingPrefix + " (slicing roles)");
        slicedRoles = createSlicedRoles();
        /* TIMING */timing.stopTimer(timingPrefix + " (slicing roles)");

        System.out.println("[START] Slicing users");
        /* TIMING */timing.startTimer(timingPrefix + " (slicing users)");
        slicedUsers = sliceUsers();
        /* TIMING */timing.stopTimer(timingPrefix + " (slicing users)");

        System.out.println("[START] Slicing admin");
        /* TIMING */timing.startTimer(timingPrefix + " (slicing admin)");
        slicedAdmin = unsliced.getAdmin();
        /* TIMING */timing.stopTimer(timingPrefix + " (slicing admin)");

        System.out.println("[START] Slicing UA");
        /* TIMING */timing.startTimer(timingPrefix + " (slicing UA)");
        slicedUA = sliceUA();
        /* TIMING */timing.stopTimer(timingPrefix + " (slicing UA)");

        System.out.println("[START] Slicing CA");
        /* TIMING */timing.startTimer(timingPrefix + " (slicing CA)");
        slicedCA = sliceCA();
        /* TIMING */timing.stopTimer(timingPrefix + " (slicing CA)");

        System.out.println("[START] Slicing CR");
        /* TIMING */timing.startTimer(timingPrefix + " (slicing CR)");
        slicedCR = sliceCR();
        /* TIMING */timing.stopTimer(timingPrefix + " (slicing CR)");

        /* TIMING */timing.stopTimer(timingPrefix + " (slicingTotal)");
        return new RBACInstance(slicedRoles, slicedUsers, slicedAdmin, slicedUA, slicedCR, slicedCA,
                unsliced.getSpec());
    }

    /*
     * Get all dependent roles and add them to the sliced roles. Also, create entry in the map to map old and new role
     * indices
     */

    public Vector<String> createSlicedRoles() {

        // Vector<String> vDepRoles = getAllDependentRoles(strRole);
        vPositiveDeps = new Vector<String>();
        vNegativeDeps = new Vector<String>();
        slicedRoles = new Vector<String>();

        this.getAllPosDeps(vPositiveDeps, vNegativeDeps, strRole);

        // Add the role in question to the list of dependent roles.
        // vDepRoles.add(strRole);
        // Add the administrative role
        // vDepRoles.add(unsliced.getRoles().lastElement());
        // vPositiveDeps.add(unsliced.getRoles().lastElement());

        // Sort and add the list of roles to sliced Roles
        Collection<String> noDup = new LinkedHashSet<String>(vPositiveDeps);

        noDup.addAll(vNegativeDeps);

        // slicedRoles.addAll(noDup);

        // noDup = new LinkedHashSet<String>(vNegativeDeps);
        slicedRoles.addAll(noDup);

        class strComparator implements Comparator<String> {

            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        }
        ;

        strComparator strComp = new strComparator();

        Collections.sort(slicedRoles, strComp);
        slicedRoles.addAll(unsliced.getAdminRoles());
        createMapping(slicedRoles);

        return slicedRoles;
    }

    /*
     * This function aggressively gets all the dependent roles in for the current role.
     */
    public Vector<String> getAllDependentRoles(String strRole) {

        Vector<String> vDepRoles = new Vector<String>();
        Vector<String> vTmpDepRoles = getDependentRoles(null, strRole);

        // Add all the dependent roles extracted from the first set.
        vDepRoles.addAll(vTmpDepRoles);

        while (!vTmpDepRoles.isEmpty()) {
            // Get first element.
            String strNextRole = vTmpDepRoles.firstElement();
            // Remove the element.
            vTmpDepRoles.remove(strNextRole);

            Vector<String> vNextDepRoles = getDependentRoles(null, strRole);
            Iterator<String> itr = vNextDepRoles.iterator();
            while (itr.hasNext()) {
                String nextRole = itr.next();
                if (!vDepRoles.contains(nextRole)) vDepRoles.add(nextRole);
            }
        }
        return vDepRoles;
    }

    /*
     * This function will get the list of dependent roles for a particular role. It works by iterating through all the
     * preconditions and finding the roles that have either a positive or negative influence.
     */
    public Vector<String> getDependentRoles(Vector<String> vDepRoles, String strRole) {

        if (vDepRoles == null) vDepRoles = new Vector<String>();

        Vector<CAEntry> vMatchingCA = unsliced.getCA().get(strRole);

        for (int i = 0; i < vMatchingCA.size(); i++) {
            CAEntry cae = vMatchingCA.get(i);
            PreCondition pcPreCond = cae.getPreConditions();

            if (pcPreCond.size() != 0) {
                for (int roleindex : pcPreCond.keySet()) {
                    String strDepRole = unsliced.getRoles().get(roleindex);
                    vDepRoles.add(strDepRole);
                }
            }
        }

        if (vDepRoles.contains(strRole)) vDepRoles.remove(strRole);

        return vDepRoles;
    }

    /* Get the list of roles that a role depends on positively */
    public void getAllPosDeps(Vector<String> vPosDeps, Vector<String> vNegDeps, String strRole) {

        Vector<String> vWorkQueue = new Vector<String>();

        vWorkQueue.add(strRole);
        while (!vWorkQueue.isEmpty()) {
            String strNextRole = vWorkQueue.firstElement(); // Get first element
                                                            // from queue.
            vWorkQueue.remove(strNextRole);
            vPosDeps.add(strNextRole);

            Vector<String> vTmpPosDeps = new Vector<String>();
            Vector<String> vTmpNegDeps = new Vector<String>();

            getPosDeps(vTmpPosDeps, vTmpNegDeps, strNextRole);

            Iterator<String> itrPos = vTmpPosDeps.iterator();
            while (itrPos.hasNext()) {
                String tmpRole = itrPos.next();
                if (!vPosDeps.contains(tmpRole)) {
                    vPosDeps.add(tmpRole);
                    vWorkQueue.add(tmpRole);
                }
            }

            Iterator<String> itrNeg = vTmpNegDeps.iterator();
            while (itrNeg.hasNext()) {
                String tmpRole = itrNeg.next();
                if (!vNegDeps.contains(tmpRole)) vNegDeps.add(tmpRole);
            }

        }
    }

    /* Helper function for getAllPosDeps */
    public void getPosDeps(Vector<String> vPosDeps, Vector<String> vNegDeps, String strRole) {

        Vector<CAEntry> vMatchingCA = unsliced.getCA().get(strRole);

        if (vMatchingCA == null) return;

        for (int i = 0; i < vMatchingCA.size(); i++) {
            CAEntry cae = vMatchingCA.get(i);
            PreCondition pcPreCond = cae.getPreConditions();

            if (pcPreCond.size() != 0) {
                for (int roleindex : pcPreCond.keySet()) {
                    String strDepRole = unsliced.getRoles().get(roleindex);

                    if (pcPreCond.getConditional(roleindex) == 1) vPosDeps.add(strDepRole);
                    else vNegDeps.add(strDepRole);
                }
            }
        }
    }

    /*
     * Create mapping for role indices
     */
    public void createMapping(Vector<String> vSliced) {

        for (int i = 0; i < vSliced.size(); i++)
            mapRoleIndex.put(unsliced.getRoles().indexOf(vSliced.get(i)), i);
    }

    /*
     * This function will reduce the number of users
     */
    public Vector<String> sliceUsers() {

        Vector<String> vSlicedUsers = new Vector<String>();

        vSlicedUsers.add(strUser);

        Iterator<String> itr = unsliced.getAdmin().iterator();
        while (itr.hasNext())
            vSlicedUsers.add(itr.next());

        return vSlicedUsers;
    }

    /*
     * Slice the UA
     */
    public Map<String, Vector<Integer>> sliceUA() throws Exception {

        Map<String, Vector<Integer>> mSlicedUA = new HashMap<String, Vector<Integer>>();

        for (int i = 0; i < slicedUsers.size(); i++) {
            String strUser = slicedUsers.get(i);
            Vector<Integer> vUserUnslicedUA = unsliced.getUA().get(strUser);
            Vector<Integer> vUserSlicedUA = new Vector<Integer>();

            if (vUserUnslicedUA != null) {
                for (int j = 0; j < vUserUnslicedUA.size(); j++) {
                    if (mapRoleIndex.containsKey(vUserUnslicedUA.get(j)))
                        vUserSlicedUA.add(mapRoleIndex.get(vUserUnslicedUA.get(j)));
                }
                mSlicedUA.put(strUser, vUserSlicedUA);
            }
        }

        return mSlicedUA;
    }

    /*
     * This function is incharge of slicing the CA rules.
     */
    Map<String, Vector<CAEntry>> sliceCA() {

        Map<String, Vector<CAEntry>> mUnSlicedCA = unsliced.getCA();
        Map<String, Vector<CAEntry>> mSlicedCA = new HashMap<String, Vector<CAEntry>>();

        // for(String strTargetRole : mUnSlicedCA.keySet())
        for (int i = 0; i < vPositiveDeps.size(); i++) {
            String strTargetRole = vPositiveDeps.get(i);
            Vector<CAEntry> vUnSlicedCA = mUnSlicedCA.get(strTargetRole);
            Vector<CAEntry> vSlicedCA = new Vector<CAEntry>();

            if (vUnSlicedCA != null) {
                Iterator<CAEntry> itr = vUnSlicedCA.iterator();

                while (itr.hasNext()) {
                    CAEntry tmpCAE = itr.next();
                    if (this.CanApplyCA(tmpCAE)) vSlicedCA.add(modifyCA(tmpCAE));
                }
            }

            if (vSlicedCA.size() > 0) mSlicedCA.put(strTargetRole, vSlicedCA);
        }

        return mSlicedCA;
    }

    /*
     * Specifies whether the CA rule applies to the sliced policy. A CA rule from the unsliced policy applies to the
     * sliced policy if and only if all conditionals in the CA involve only the roles in the sliced policy. In such
     * cases, the conditionals can be simply modified by appropriately changing the indices.
     */
    boolean CanApplyCA(CAEntry cae) {

        PreCondition pcUnSlicedCond = cae.getPreConditions();

        if (pcUnSlicedCond.size() > 0) {
            for (int i : pcUnSlicedCond.keySet()) {
                if (!mapRoleIndex.containsKey(i)) return false;
            }
        }

        return true;
    }

    /*
     * This function is responsible for modifying a CA rule. When the number of roles in the policy are reduced, then
     * the preconditions in the NuSMV spec have to be modified. This is because the policy has fewer entries in the
     * BitVector.
     */
    CAEntry modifyCA(CAEntry cae) {

        PreCondition UnSlicedCond = cae.getPreConditions();
        PreCondition SlicedCond = new PreCondition();

        if (UnSlicedCond.size() > 0) {
            for (int i : UnSlicedCond.keySet()) {
                int iNewIndex = mapRoleIndex.get(i); // getNewRoleIndex(i);

                if (iNewIndex != -1) SlicedCond.addConditional(iNewIndex, UnSlicedCond.getConditional(i));
            }
        }

        CAEntry newCAE = new CAEntry(cae.getAdminRole(), SlicedCond, cae.getRole());
        return newCAE;
    }

    /*
     * Modify CR; Drop of all CR rules that do not matter.
     */
    public Map<String, Vector<CREntry>> sliceCR() {

        Map<String, Vector<CREntry>> mUnSlicedCR = unsliced.getCR();
        Map<String, Vector<CREntry>> mSlicedCR = new HashMap<String, Vector<CREntry>>();

        // for(String strTargetRole : mUnSlicedCR.keySet())
        for (int i = 0; i < vNegativeDeps.size(); i++) {
            String strTargetRole = vNegativeDeps.get(i);
            Vector<CREntry> vUnSlicedCR = mUnSlicedCR.get(strTargetRole);

            if (vUnSlicedCR != null) {
                if (vUnSlicedCR.size() > 0) {
                    Vector<CREntry> vSlicedCR = new Vector<CREntry>();

                    vSlicedCR.addAll(vUnSlicedCR);
                    mSlicedCR.put(strTargetRole, vSlicedCR);
                }
            }
        }
        return mSlicedCR;
    }

    /*
     * These are utility functions
     */

    /*
     * NewRoleIndex returns the the roleindex for a role in the sliced policy, when queried with the roleindex of the
     * role in the old policy
     */
    public int getNewRoleIndex(int inOldIndex) {

        return mapRoleIndex.get(inOldIndex);
    }

    /*
     * This function returns the roleindex for a role in the old policy, when queried using the roleindex in the new
     * policy.
     */
    public int getOldRoleIndex(int inNewIndex) {

        for (int i = 0; i < mapRoleIndex.size(); i++) {
            if (mapRoleIndex.get(i) == inNewIndex) return i;
        }

        return -1;
    }

}
