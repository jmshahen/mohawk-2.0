package mohawk;

public enum OptionString {
    HELP("help"), AUTHORS("authors"), VERSION("version"), CHECKNUSMV("checknusmv"), LOGLEVEL("loglevel"), LOGFILE(
            "logfile"), LOGFOLDER("logfolder"), NOHEADER("noheader"), RESULTSFILE("output"), MAXW("maxw"), LINESTR(
                    "linstr"), SPECFILE("input"), SMVFILE("smvfile"), SPECEXT("specext"), BULK("bulk"), NOSLICING(
                            "noslicing"), SLICEQUERY("slicequery"), MODE("mode"), TIMEOUT(
                                    "timeout"), RUN("run"), NUSMVPATH("nusmv"), SKIPREFINE("skiprefine");

    private String _str;

    private OptionString(String s) {
        _str = s;
    }

    @Override
    public String toString() {
        return _str;
    }

    /** Returns the commandline equivalent with the hyphen and a space following: AUTHORS -> "-authors "
     * 
     * @return */
    public String c() {
        return "-" + _str + " ";
    }
}
