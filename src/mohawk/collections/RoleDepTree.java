/**
 * 
 */
package mohawk.collections;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/** @author kjayaram */
public class RoleDepTree {

    public RoleDepTree() {
        mPriorityQueue = new HashMap<Integer, Set<String>>();
        mRoleLevel = new HashMap<String, Integer>();
        mAllRoles = new HashSet<String>();
    }

    public void addRole(String strRole, int level) {

        if (!mAllRoles.contains(strRole)) {

            Set<String> setRoles = mPriorityQueue.get(level);

            if (setRoles == null) setRoles = new HashSet<String>();

            setRoles.add(strRole);
            mPriorityQueue.put(level, setRoles);
            mRoleLevel.put(strRole, level);
            mAllRoles.add(strRole);
        }
    }

    public int getRoleLevel(String strRole) {
        return mRoleLevel.get(strRole);
    }

    public Set<String> getRoles(int level) {
        return mPriorityQueue.get(level);
    }

    private Map<Integer, Set<String>> mPriorityQueue;
    private Map<String, Integer> mRoleLevel;
    private Set<String> mAllRoles;
}
