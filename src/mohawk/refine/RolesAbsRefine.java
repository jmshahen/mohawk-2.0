/**
 * 
 */
package mohawk.refine;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

import mohawk.collections.RoleDepTree;
import mohawk.global.nusmv.NuSMV;
import mohawk.global.pieces.mohawk.*;
import mohawk.global.results.ExecutionResult;
import mohawk.global.timing.MohawkTiming;
import mohawk.math.CalculateDiameter;
import mohawk.output.WriteNuSMV;
import mohawk.output.WriteRBACSpec;
import mohawk.rbac.RBACInstance;
import mohawk.singleton.MohawkSettings;

/** @author kjayaram */
public class RolesAbsRefine {
    public final static Logger logger = Logger.getLogger("mohawk");

    private RoleDepTree rdeptree;
    private RBACInstance unsliced;
    private RBACInstance nextinstance;
    private Vector<String> allroles;
    private Vector<String> nextsetroles;
    private Map<String, Vector<CAEntry>> nextCA;
    private Map<String, Vector<CREntry>> nextCR;
    private Map<String, Vector<Integer>> nextUA;
    private Vector<String> nextUsers;
    private Vector<String> nextSpec;
    private Vector<String> nextAdmin;
    private Map<Integer, Integer> mMapping;
    private Map<String, Vector<CAEntry>> mAllCA;
    private Map<String, Vector<CREntry>> mAllCR;
    private Map<String, Vector<Integer>> mAllUA;
    public int refinementstep;
    private String strRole;
    private int k;
    private int lastpriority;
    private NuSMVMode mode;// mode

    public boolean skipSMVFile = false;
    private Integer returnValue = null;

    // Timing
    public MohawkTiming timing;
    public String timingPrefix;

    public RolesAbsRefine(RBACInstance inRbac, MohawkTiming timing, String timingPrefix) {
        this.timing = timing;
        this.timingPrefix = timingPrefix;

        mode = NuSMVMode.BMC;// Default mode is BMC
        k = 2;
        lastpriority = 0;
        unsliced = inRbac;
        strRole = unsliced.getSpec().get(1);
        allroles = unsliced.getRoles();
        mAllCA = unsliced.getCA();
        mAllCR = unsliced.getCR();
        mAllUA = unsliced.getUA();
        buildTree();
    }

    public String getSpecRole() {
        return strRole;
    }

    public RBACInstance getInputRBACInstance() {
        return unsliced;
    }

    private Set<String> getAdminRoles() {
        Set<String> sAdminRoles = new HashSet<String>();
        Vector<String> Admin = unsliced.getAdmin();

        for (int i = 0; i < Admin.size(); i++) {
            String strUser = Admin.get(i);
            Vector<Integer> vUA = mAllUA.get(strUser);

            for (int j = 0; j < vUA.size(); j++) {
                int roleindex = vUA.get(j);
                String strRole = this.allroles.get(roleindex);
                sAdminRoles.add(strRole);
            }
        }

        return sAdminRoles;
    }

    public Boolean getResult(RBACInstance curInstance, Integer fileno) throws Exception {
        WriteNuSMV nusmv = new WriteNuSMV("logs/smvinstancefile" + fileno + ".txt", timing, timingPrefix);
        nusmv.fillAttributes(curInstance);

        if (!skipSMVFile) {
            nusmv.writeFile();
        }

        // NuSMV bmc
        Boolean result;
        NuSMV runNuSMV = new NuSMV(MohawkSettings.getInstance().getNuSMV_filepath());
        if (mode == NuSMVMode.BMC) {
            // CalculateDiameter diameter = new CalculateDiameter(); // NEVER USED
            int bound = CalculateDiameter.GetDiameter(curInstance);
            System.out.println(
                    String.format("Estimate Diameter - No of Roles %d Diameter %d", curInstance.getNumRoles(), bound));

            if (skipSMVFile) {
                result = runNuSMV.BMC(bound, nusmv.getNuSMVCode());
            } else {
                result = runNuSMV.BMCFile(bound, nusmv.getFilename());
            }
        } else {

            if (skipSMVFile) {
                result = runNuSMV.SMC(nusmv.getNuSMVCode());
            } else {
                result = runNuSMV.SMCFile(nusmv.getFilename());
            }
        }

        returnValue = runNuSMV.execProcess.exitValue();

        return result;
    }

    public void getAllPosDeps(Vector<String> vPosDeps, Vector<String> vNegDeps, String strRole) {

        Vector<String> vWorkQueue = new Vector<String>();

        vWorkQueue.add(strRole);

        rdeptree.addRole(strRole, 0);

        while (!vWorkQueue.isEmpty()) {
            String strNextRole = vWorkQueue.firstElement();// Get first element
                                                           // from queue.
            vWorkQueue.remove(strNextRole);

            Vector<String> vTmpPosDeps = new Vector<String>();
            Vector<String> vTmpNegDeps = new Vector<String>();

            getPosDeps(vTmpPosDeps, vTmpNegDeps, strNextRole);
            int nextLevel = rdeptree.getRoleLevel(strNextRole) + 1;

            Iterator<String> itrPos = vTmpPosDeps.iterator();
            while (itrPos.hasNext()) {
                String tmpRole = itrPos.next();

                if (!vPosDeps.contains(tmpRole)) {
                    vPosDeps.add(tmpRole);
                    vWorkQueue.add(tmpRole);
                    rdeptree.addRole(tmpRole, nextLevel);
                }
            }

            Iterator<String> itrNeg = vTmpNegDeps.iterator();
            while (itrNeg.hasNext()) {
                String tmpRole = itrNeg.next();
                if (!vNegDeps.contains(tmpRole)) {
                    vNegDeps.add(tmpRole);
                    rdeptree.addRole(tmpRole, nextLevel);
                }
            }

        }
    }

    /* Helper function for getAllPosDeps */
    public void getPosDeps(Vector<String> vPosDeps, Vector<String> vNegDeps, String strRole) {

        Vector<CAEntry> vMatchingCA = unsliced.getCA().get(strRole);

        if (vMatchingCA == null) return;

        for (int i = 0; i < vMatchingCA.size(); i++) {
            CAEntry cae = vMatchingCA.get(i);
            PreCondition pcPreCond = cae.getPreConditions();

            if (pcPreCond.size() != 0) {
                for (int roleindex : pcPreCond.keySet()) {
                    String strDepRole = unsliced.getRoles().get(roleindex);

                    if (pcPreCond.getConditional(roleindex) == 1) vPosDeps.add(strDepRole);
                    else vNegDeps.add(strDepRole);
                }
            }
        }
    }

    public void setMode(NuSMVMode curmode) {
        this.mode = curmode;
    }

    public boolean refineRoles() {
        if (nextsetroles.size() == allroles.size()) { return false; }

        // Set<String> incrementRoles = rdeptree.getRoles(refinementstep);
        Set<String> incrementRoles = new HashSet<String>();// = rdeptree.getRoles(refinementstep);
        for (int i = 0; i < k; i++) {
            Set<String> sNextQueue = this.rdeptree.getRoles(lastpriority);
            if (sNextQueue != null) {
                incrementRoles.addAll(sNextQueue);
                lastpriority++;
            } else {
                break;
            }
        }

        if (incrementRoles.size() == 0) {
            return false;
        } else {
            nextsetroles.addAll(incrementRoles);
        }

        refinementstep++;

        return true;
    }

    public ExecutionResult absrefineloop() {
        Integer fileno = 1;

        try {
            while (refine()) {
                try {
                    WriteRBACSpec writer = new WriteRBACSpec();
                    writer.Write2File(nextinstance, "logs/rbacinstancefile" + fileno + ".txt");
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                System.out.printf("\n\n-----\n Starting Refinement Step %d, with %d Roles and %d Rules\n", fileno,
                        nextinstance.getNumRoles(), nextinstance.getNumRules());

                Boolean result = getResult(nextinstance, fileno);

                if (result) {
                    System.out.println("Found counter example in step " + (fileno));
                    WriteMapping(nextinstance, "ROLEMAPPING");
                    return ExecutionResult.GOAL_REACHABLE;
                } else {
                    System.out.println("No counter example found in refinement step " + fileno);
                }

                fileno++;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ExecutionResult.ERROR_OCCURRED;
        }

        System.out.println("\n\n----\n No counter example found in " + (fileno - 1) + " refinement steps");
        return ExecutionResult.GOAL_UNREACHABLE;
    }

    /** A quick function to see if this will get rid of the NuSMV error that is given when trying to use the SMV code
     * generated by RBAC2SMV.java
     * 
     * @author Jonathan Shahen
     * @return */
    public RBACInstance refineOnly() {
        refine();

        return nextinstance;
    }

    public void WriteMapping(RBACInstance inrbacinstance, String filename) throws IOException {

        FileWriter file = new FileWriter(filename);
        Vector<String> vRoles = inrbacinstance.getRoles();

        for (int i = 0; i < vRoles.size(); i++) {
            file.write(i + " " + vRoles.get(i) + "\n");
        }
        file.close();
    }

    private void firstabstraction() {

        Set<String> sFirstSetRoles = new HashSet<String>();
        for (int i = 0; i < k; i++) {
            Set<String> sNextQueue = this.rdeptree.getRoles(lastpriority);
            if (sNextQueue != null) {
                sFirstSetRoles.addAll(sNextQueue);
                lastpriority++;
            } else break;
        }

        nextsetroles = new Vector<String>();
        // nextsetroles.add(strRole);
        nextsetroles.addAll(sFirstSetRoles);
        nextsetroles.addAll(getAdminRoles());
        createmapping();

        nextSpec = unsliced.getSpec();
        nextUsers = unsliced.getUsers();
        nextAdmin = unsliced.getAdmin();

        try {
            sliceUA();
        } catch (Exception e) {
            e.printStackTrace();
        }

        sliceCA();
        sliceCR();

        nextinstance = new RBACInstance(nextsetroles, nextUsers, nextAdmin, nextUA, nextCR, nextCA, nextSpec);

        this.refinementstep = 1;
    }

    private void sliceUA() throws Exception {

        if (nextUA == null) nextUA = new HashMap<String, Vector<Integer>>();

        for (int i = 0; i < nextUsers.size(); i++) {
            String strUser = nextUsers.get(i);
            Vector<Integer> vUserUnslicedUA = mAllUA.get(strUser);
            Vector<Integer> vUserSlicedUA = new Vector<Integer>();

            if (vUserUnslicedUA != null) {
                for (int j = 0; j < vUserUnslicedUA.size(); j++) {
                    int oldindex = vUserUnslicedUA.get(j);

                    if (mMapping.containsKey(oldindex)) {
                        int newindex = mMapping.get(oldindex);
                        vUserSlicedUA.add(newindex);
                    }
                }
                nextUA.put(strUser, vUserSlicedUA);
            }
        }
    }

    /*
     * Modify CR; Drop of all CR rules that do not matter.
     */
    private void sliceCR() {

        if (nextCR == null) nextCR = new HashMap<String, Vector<CREntry>>();
        else nextCR.clear();

        // for(String strTargetRole : mUnSlicedCR.keySet())
        for (int i = 0; i < nextsetroles.size(); i++) {
            String strTargetRole = nextsetroles.get(i);
            Vector<CREntry> vUnSlicedCR = mAllCR.get(strTargetRole);

            if (vUnSlicedCR != null) {
                if (vUnSlicedCR.size() > 0) {
                    Vector<CREntry> vSlicedCR = new Vector<CREntry>();
                    vSlicedCR.addAll(vUnSlicedCR);
                    nextCR.put(strTargetRole, vSlicedCR);
                }
            }
        }
    }

    /*
     * This function is incharge of slicing the CA rules.
     */
    private void sliceCA() {

        if (nextCA == null) nextCA = new HashMap<String, Vector<CAEntry>>();
        else nextCA.clear();

        // for(String strTargetRole : mUnSlicedCA.keySet())
        for (int i = 0; i < nextsetroles.size(); i++) {
            String strTargetRole = nextsetroles.get(i);
            Vector<CAEntry> vUnSlicedCA = mAllCA.get(strTargetRole);
            Vector<CAEntry> vSlicedCA = new Vector<CAEntry>();

            if (vUnSlicedCA != null) {
                Iterator<CAEntry> itr = vUnSlicedCA.iterator();

                while (itr.hasNext()) {
                    CAEntry tmpCAE = itr.next();
                    if (this.CanApplyCA(tmpCAE)) vSlicedCA.add(modifyCA(tmpCAE));
                }
            }

            if (vSlicedCA.size() > 0) nextCA.put(strTargetRole, vSlicedCA);
        }
    }

    /*
     * Specifies whether the CA rule applies to the sliced policy. A CA rule from the unsliced policy applies to the
     * sliced policy if and only if all conditionals in the CA involve only the roles in the sliced policy. In such
     * cases, the conditionals can be simply modified by appropriately changing the indices.
     */
    private boolean CanApplyCA(CAEntry cae) {

        PreCondition pcUnSlicedCond = cae.getPreConditions();

        if (pcUnSlicedCond.size() > 0) {
            for (int i : pcUnSlicedCond.keySet()) {
                if (!mMapping.containsKey(i)) return false;
            }
        }

        return true;
    }

    /*
     * This function is responsible for modifying a CA rule. When the number of roles in the policy are reduced, then
     * the preconditions in the NuSMV spec have to be modified. This is because the policy has fewer entries in the
     * BitVector.
     */
    private CAEntry modifyCA(CAEntry cae) {

        PreCondition UnSlicedCond = cae.getPreConditions();
        PreCondition SlicedCond = new PreCondition();

        if (UnSlicedCond.size() > 0) {
            for (int i : UnSlicedCond.keySet()) {
                if (mMapping.containsKey(i)) {
                    int iNewIndex = mMapping.get(i);// getNewRoleIndex(i);
                    SlicedCond.addConditional(iNewIndex, UnSlicedCond.getConditional(i));
                }
            }
        }

        CAEntry newCAE = new CAEntry(cae.getAdminRole(), SlicedCond, cae.getRole());
        return newCAE;
    }

    private void createmapping() {
        if (mMapping == null) mMapping = new HashMap<Integer, Integer>();
        else mMapping.clear();

        for (int i = 0; i < nextsetroles.size(); i++) {
            String strRole = nextsetroles.get(i);
            mMapping.put(allroles.indexOf(strRole), nextsetroles.indexOf(strRole));
        }
    }

    // Build the role dependency tree
    private void buildTree() {
        Vector<String> vPosDeps = new Vector<String>();
        Vector<String> vNegDeps = new Vector<String>();
        rdeptree = new RoleDepTree();

        String strRole = unsliced.getSpec().get(1);
        getAllPosDeps(vPosDeps, vNegDeps, strRole);

    }

    private boolean refine() {
        boolean result;

        if (nextinstance == null) {
            firstabstraction();
            return true;
        } else {
            result = refineRoles();
            if (result) {
                createmapping();
                try {
                    sliceUA();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                sliceCA();
                sliceCR();
                nextinstance = new RBACInstance(nextsetroles, nextUsers, nextAdmin, nextUA, nextCR, nextCA, nextSpec);
                System.out.println("No of roles : " + nextsetroles.size());
                System.out.println("No of CA Rules : " + nextCA.size());

            }
        }

        return result;
    }

    public String getReturnValue() {
        if (returnValue != null) { return "" + returnValue; }
        return "No Return Value Found";
    }
}
