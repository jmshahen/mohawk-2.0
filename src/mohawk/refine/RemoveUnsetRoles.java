package mohawk.refine;

import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

import mohawk.global.pieces.mohawk.CAEntry;
import mohawk.global.pieces.mohawk.CREntry;
import mohawk.global.timing.MohawkTiming;
import mohawk.output.WriteRBACSpec;
import mohawk.rbac.RBACInstance;

public class RemoveUnsetRoles {
    public static final Logger logger = Logger.getLogger("mohawk");
    private RBACInstance inst;
    private MohawkTiming timing;
    private String timingPrefix;

    // target role name -> list of rules
    private Map<String, Vector<CAEntry>> mCA;
    // target role name -> list of rules
    private Map<String, Vector<CREntry>> mCR;
    // user name -> list of roles using their index
    private Map<String, Vector<Integer>> mUA;
    private boolean debugMsg = false;

    public RemoveUnsetRoles(RBACInstance in, MohawkTiming timing, String timingPrefix) {
        inst = in;
        this.timing = timing;
        this.timingPrefix = timingPrefix;

        mCA = new HashMap<String, Vector<CAEntry>>(in.getCA());
        mCR = new HashMap<String, Vector<CREntry>>(in.getCR());
        mUA = new HashMap<String, Vector<Integer>>();
    }

    /** Slices away the Unset (not used in a single rule as the target for CA) Roles and replaces them with literals
     * IF the precondition requires a Unset Role then it is defaulted to FALSE (and thus can remove the whole rule)
     * IF the precondition requires to NOt have a Unset Role then the role is substituted to TRUE
     * 
     * @return whether an error occurred
     * @throws Exception */
    public RBACInstance getOptimizedRBAC() throws Exception {
        String timerKey = timingPrefix + "->RemoveUnsetRoles.slice()";
        /* timing */timing.startTimer(timerKey);

        Set<String> targets = new HashSet<String>(inst.getTargetRolesSet()); // new list of roles
        targets.addAll(inst.getAdminRoles());
        Vector<String> vRoles = new Vector<String>(targets);

        Set<String> unset = inst.getRolesSet();
        unset.removeAll(targets);

        // If there are no roles to remove, just return the original RBAC instance
        if (unset.isEmpty()) { return inst; }

        HashSet<Integer> removeRolesIndex = new HashSet<Integer>(unset.size());

        for (String role : unset) {
            logger.fine("[RemoveUnsetRoles] Removing role: " + role + " from all Rules");
            removeRolesIndex.add(inst.getRoleIndex(role));
        }

        Vector<String> caRoles = new Vector<String>(mCA.keySet());
        for (int j = mCA.keySet().size() - 1; j >= 0; j--) {
            Vector<CAEntry> mCa = mCA.get(caRoles.get(j));

            for (int i = mCa.size() - 1; i >= 0; i--) {
                CAEntry ca = mCa.get(i);

                if (ca.updateRule(removeRolesIndex, vRoles, inst.getRoleMap()) == false) {
                    logger.fine("[RemoveUnsetRoles] Removing CA rule: " + ca + " from all Rules");
                    mCa.remove(i);
                }
            }

            if (mCa.isEmpty()) {
                mCA.remove(caRoles.get(j));
            }
        }

        Vector<String> crRoles = new Vector<String>(mCR.keySet());
        for (int i = crRoles.size() - 1; i >= 0; i--) {
            // Remove the CR rule if the target role is in unset
            if (unset.contains(crRoles.get(i))) {
                logger.fine("[RemoveUnsetRoles] Removing all CR rules for: " + crRoles.get(i));
                mCR.remove(crRoles.get(i));
            }

        }

        // Update mUA to point to the new role indexes
        for (String user : inst.getUA().keySet()) {
            Vector<Integer> roles = inst.getUA().get(user);
            Vector<Integer> newRoles = new Vector<Integer>(roles.size());

            for (Integer role : roles) {
                newRoles.add(vRoles.indexOf(inst.getRoleFromIndex(role)));
            }

            mUA.put(user, newRoles);
        }

        if (debugMsg == true) {
            System.out.println("vRoles: " + vRoles);
            System.out.println("unset: " + unset);
            System.out.println("mUA: " + mUA);
            System.out.println("mCR: " + mCR);
            System.out.println("mCA: " + mCA);
            System.out.println("SPEC: " + inst.getSpec());
        }

        /* timing */timing.stopTimer(timerKey);
        RBACInstance bb = new RBACInstance(vRoles, inst.getUsers(), inst.getAdmin(), mUA, mCR, mCA, inst.getSpec());

        if (debugMsg == true) {
            try {
                WriteRBACSpec writer = new WriteRBACSpec();
                writer.Write2File(bb, "logs/testingRemoveUnsetRoles.mohawk");
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        return bb;
    }
}
