package mohawk;

public class Mohawk {

    public static void main(String[] args) {
        MohawkInstance mohawk = new MohawkInstance();

        mohawk.run(args);
    }

}
