package mohawk.helper;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

import mohawk.singleton.MohawkSettings;

public class SMVSpecHelper {
    public final static Logger logger = Logger.getLogger("mohawk");
    private MohawkSettings settings = MohawkSettings.getInstance();

    public ArrayList<File> specFiles = new ArrayList<File>();

    public void loadSpecFiles() throws IOException {
        if (settings.bulk == true) {
            this.loadSpecFilesFromFolder(settings.specFile);
        } else {
            this.addSpecFile(settings.specFile);
        }
    }

    public void loadSpecFilesFromFolder(String path) {
        if (path == null || path == "") {
            logger.severe("[ERROR] No SPEC Folder provided");
        }

        File folder = new File(path);

        if (!folder.exists()) {
            logger.severe("[ERROR] Spec Folder: '" + path + "' does not exists!");
        }
        if (!folder.isDirectory()) {
            logger.severe("[ERROR] Spec Folder: '" + path + "' is not a folder and the 'bulk' option is present. "
                    + "Try removing the '-bulk' option if you wish to use "
                    + "a specific file, or change the option to point to a folder");
        }

        for (File f : folder.listFiles()) {
            if (f.getName().endsWith(settings.specFileExt)) {
                logger.fine("Adding file to specFiles: " + f.getAbsolutePath());
                specFiles.add(f);
            }
        }

        // Need to sort for linux
        Collections.sort(specFiles, new Comparator<File>() {
            public int compare(File p1, File p2) {
                return p1.getName().compareToIgnoreCase(p2.getName());
            }
        });
    }

    public void addSpecFile(String path) throws IOException {
        if (path == null || path.equals("")) {
            logger.severe("[ERROR] No SPEC File provided");
        }

        File file2 = new File(path);
        if (!file2.exists()) {
            logger.severe("[ERROR] Spec File: '" + path + "' does not exists!");
        }
        if (!file2.isFile()) {
            logger.severe("[ERROR] Spec File: '" + path + "' is not a file and the 'bulk' option was not set. "
                    + "Try setting the '-bulk' option if you wish to search "
                    + "through the folder, or change the option to point to a specific file");
        }

        logger.info("[FILE IO] Using SPEC File: " + file2.getCanonicalPath());
        specFiles.add(file2);
    }

    public File getSmvFile(File specFile) {
        File nusmvFile = null;
        if (settings.smvDeleteFile) {
            try {
                nusmvFile = File.createTempFile("smvTempFile", ".smv");
            } catch (IOException e) {
                logger.severe("[ERROR] Unable to create a temporary SMV file in the current working directory: "
                        + new File("").getAbsoluteFile());
            }
            return nusmvFile;
        }

        if (settings.bulk) {
            nusmvFile = new File(specFile.getAbsolutePath() + ".smv");
        } else {
            nusmvFile = new File(settings.smvFilepath);
        }

        if (!nusmvFile.exists()) {
            try {
                nusmvFile.createNewFile();
            } catch (IOException e) {
                logger.severe("[ERROR] Unable to create a SMV file in the current working directory: "
                        + new File("").getAbsoluteFile());
            }
        }
        return nusmvFile;
    }
}
