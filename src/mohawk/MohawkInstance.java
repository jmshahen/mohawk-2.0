package mohawk;

import java.io.*;
import java.util.logging.*;

import org.apache.commons.cli.*;
import org.apache.commons.lang3.StringUtils;

import mohawk.global.formatter.MohawkCSVFileFormatter;
import mohawk.global.formatter.MohawkConsoleFormatter;
import mohawk.global.results.MohawkResults;
import mohawk.global.timing.MohawkTiming;
import mohawk.helper.SMVSpecHelper;
import mohawk.singleton.MohawkSettings;
import mohawk.testing.TestingSuite;

/** @author Jonathan Shahen */

public class MohawkInstance {
    private final String VERSION = "v2.3.1";
    private static final String AUTHORS = "Jonathan Shahen <jmshahen [AT] uwaterloo [DOT] ca>; Karthick Jayaraman";

    // Logger Fields
    public static final Logger logger = Logger.getLogger("mohawk");
    private MohawkSettings settings = MohawkSettings.getInstance();

    public ConsoleHandler consoleHandler = new ConsoleHandler();
    public FileHandler fileHandler;

    public int run(String[] args) {
        try {
            Options options = new Options();
            CommandLineParser parser = new BasicParser();

            setupOptions(options);

            CommandLine cmd = parser.parse(options, args);

            setupLoggerOptions(cmd, options);

            if (setupReturnImmediatelyOptions(cmd, options)) { return 0; }

            setupUserPreferenceOptions(cmd, options);

            setupSmvSpecOptions(cmd, options);

            setupResultOptions(cmd, options);

            // Execute the test cases
            if (cmd.hasOption(OptionString.RUN.toString())) {
                logger.info("[ACTION] Run paramter detected");
                settings.results = new MohawkResults();
                settings.timing = new MohawkTiming();
                settings.smvHelper = new SMVSpecHelper();
                settings.tests = new TestingSuite();

                if (cmd.hasOption(OptionString.NOSLICING.toString())) {
                    logger.fine("[OPTION] RBAC Slicing is OFF");
                    settings.sliceRBAC = false;
                } else {
                    logger.fine("[OPTION] RBAC Slicing is ON");
                }

                if (cmd.hasOption(OptionString.SLICEQUERY.toString())) {
                    logger.fine("[OPTION] RBAC Query Slicing is ON");
                    settings.sliceRBACQuery = true;
                } else {
                    logger.fine("[OPTION] RBAC Query Slicing is OFF");
                }

                String runVal = cmd.getOptionValue(OptionString.RUN.toString());
                switch (runVal) {
                    case "all" :
                        logger.info("[ALL ACTION] Will convert and then execute the testcase provided");
                        settings.tests.runTests();
                        break;
                    case "smv" :
                        logger.info("[SMV ACTION] Will only convert the testcase provided");
                        settings.tests.onlyConvertSpecToSmvFormat();
                        break;
                    case "dia" :
                        logger.info("[DIA ACTION] Will only calculate the estimated diameter of the testcase provided");
                        settings.tests.calculateDiameter();
                        break;
                    default :
                        logger.severe("The Run Option '" + runVal + "' has not been implemented. "
                                + "Please see use 'mohawk -help' to see which Run Options have been implemented");
                }

                settings.tests.done();
                logger.info("[TIMING] " + settings.timing.toString());
                settings.timing.writeOut(new File(settings.timingResultsFile), false);
            }
        } catch (ParseException e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            logger.severe(errors.toString());
            logger.severe(e.getMessage());

            return -2;
        } catch (Exception e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            logger.severe(errors.toString());
            logger.severe(e.getMessage());

            return -1;
        }

        logger.info("[EOF] Mohawk is done running");
        for (Handler h : logger.getHandlers()) {
            h.close();// must call h.close or a .LCK file will remain.
        }

        return 0;
    }

    public Level getLoggerLevel() {
        return settings.LoggerLevel;
    }

    public TestingSuite getTestingSuite() {
        return settings.tests;
    }

    public void setLoggerLevel(Level loggerLevel) {
        settings.LoggerLevel = loggerLevel;
    }

    @SuppressWarnings("static-access")
    public void setupOptions(Options options) {
        // Add Information Options
        options.addOption(OptionString.HELP.toString(), false, "Print this message");
        options.addOption(OptionString.VERSION.toString(), false, "Prints the version (" + VERSION + ") information");
        options.addOption(OptionString.CHECKNUSMV.toString(), false,
                "Checks that NuSMV is on the system and displays which version is installed");

        // Add Logging Level Options
        options.addOption(OptionBuilder.withArgName("quiet|warning|info|fine|debug")
                .withDescription("default is info level").hasArg().create(OptionString.LOGLEVEL.toString()));

        options.addOption(OptionBuilder.withArgName("logfile|'n'|'u'")
                .withDescription("The filepath where the log file should be created; "
                        + "No file will be created when equal to 'n'; "
                        + "A unique filename will be created when equal to 'u'; " + "default it creates a log called '"
                        + settings.Logger_filepath + "'")
                .hasArg().create(OptionString.LOGFILE.toString()));

        options.addOption(OptionBuilder.withArgName("folderpath")
                .withDescription("The folderpath where the log file should be created; "
                        + "default it creates in the current directory")
                .hasArg().create(OptionString.LOGFOLDER.toString()));

        options.addOption(OptionString.NOHEADER.toString(), false,
                "Does not write the CSV file header to the output log");

        options.addOption(
                OptionBuilder.withArgName("csvfile").withDescription("The file where the result should be stored")
                        .hasArg().create(OptionString.RESULTSFILE.toString()));

        // custom Console Logging Options
        options.addOption(
                OptionBuilder.withArgName("num").withDescription("The maximum width of the console (default 120)")
                        .hasArg().create(OptionString.MAXW.toString()));
        options.addOption(OptionBuilder.withArgName("string")
                .withDescription("The new line string when wrapping a long line (default '\\n    ')").hasArg()
                .create(OptionString.LINESTR.toString()));

        // Add File IO Options
        options.addOption(OptionBuilder.withArgName("file|folder")
                .withDescription("Path to the RBAC Spec file or Folder if the 'bulk' option is set").hasArg()
                .create(OptionString.SPECFILE.toString()));

        options.addOption(OptionBuilder.withArgName("filepath")
                .withDescription("Path to the NuSMV program file (defaults to 'NuSMV')").hasArg()
                .create(OptionString.NUSMVPATH.toString()));

        options.addOption(OptionBuilder.withArgName("smvfile|'n'")
                .withDescription("The file/folder path where the SMV file(s) should be created; "
                        + "Only temporary files will be used when equal to 'n'; "
                        + "by default it creates a SMV called '" + settings.smvFilepath + "'")
                .hasArg().create(OptionString.SMVFILE.toString()));

        options.addOption(OptionBuilder.withArgName("extension")
                .withDescription(
                        "File extention used when searching for SPEC files when the 'bulk' option is used. Default:'"
                                + settings.specFileExt + "'")
                .hasArg().create(OptionString.SPECEXT.toString()));

        // Add Functional Options
        options.addOption(OptionString.BULK.toString(), false,
                "Use the folder that rbacspec points to and run against all *.spec");
        options.addOption(OptionString.NOSLICING.toString(), false,
                "Turns off the slicing of the RBAC Input Spec file (Slices by default)");
        options.addOption(OptionString.SLICEQUERY.toString(), false,
                "Turns on the slicing of the RBAC Input Spec Query (Does not slice by default)");
        options.addOption(OptionString.SKIPREFINE.toString(), false,
                "Turns off the refining step of the RBAC Input Spec (Refines by default)");

        options.addOption(OptionBuilder.withArgName("bmc|smc|both")
                .withDescription("Uses the Bound Estimation Checker when equal to 'bmc'; "
                        + "Uses Symbolic Model Checking when equal to 'smc'")
                .hasArg().create(OptionString.MODE.toString()));

        options.addOption(OptionBuilder.withArgName("seconds").withDescription(
                "The timeout time in seconds for Mohawk's refinement loop. Default: " + settings.TIMEOUT_SECONDS)
                .hasArg().create(OptionString.TIMEOUT.toString()));

        // Add Actionable Options
        options.addOption(OptionBuilder.withArgName("all|smv|dia")
                .withDescription("Runs the whole model checker when equal to 'all'; "
                        + "Runs only the SMV conversion when equal to 'smv'; "
                        + "Calculates the diameter when equal to 'dia'")
                .hasArg().create(OptionString.RUN.toString()));
    }

    public void printHelp(CommandLine cmd, Options options) throws Exception {
        if (cmd.hasOption(OptionString.MAXW.toString())) {
            try {
                Integer maxw = Integer.decode(cmd.getOptionValue(OptionString.MAXW.toString()));
                printHelp(options, maxw);
            } catch (Exception e) {
                printHelp(options, 80);

                e.printStackTrace();
                throw new Exception("An error occured when trying to print out the help options!");
            }
        } else {
            printHelp(options, 80);
        }
    }

    public void printHelp(Options options, int maxw) {
        HelpFormatter f = new HelpFormatter();
        f.printHelp(maxw, "mohawk",
                StringUtils.repeat("-", maxw) + "\nAuthors: " + AUTHORS + "\n" + StringUtils.repeat("-", 20), options,
                StringUtils.repeat("-", maxw), true);
    }

    private void setupSmvSpecOptions(CommandLine cmd, Options options) {
        // Load in SPEC Files
        // SMV File
        if (cmd.hasOption(OptionString.SMVFILE.toString())) {
            if (cmd.getOptionValue(OptionString.SMVFILE.toString()).equals("n")) {
                logger.fine("[OPTION] Using temporary SMV Files - will be deleted after each use");
                settings.smvDeleteFile = true;
            } else {
                logger.fine(
                        "[OPTION] Using a specific SMV File: " + cmd.getOptionValue(OptionString.SMVFILE.toString()));
                settings.smvFilepath = cmd.getOptionValue(OptionString.SMVFILE.toString());
            }
        } else {
            logger.fine("[OPTION] No SMV Filename included, saving file under: " + settings.smvFilepath);
        }

        // Turn off refining of the RBAC File
        if (cmd.hasOption(OptionString.SKIPREFINE.toString())) {
            logger.fine("[OPTION] RBAC Role Refining is OFF");
            settings.skipRefine = true;
        } else {
            logger.fine("[OPTION] RBAC Role Refining is ON");
            settings.skipRefine = false;
        }

        // Grab the SPEC file
        if (cmd.hasOption(OptionString.SPECFILE.toString())) {
            logger.fine("[OPTION] Using a specific SPEC File: " + cmd.getOptionValue(OptionString.SPECFILE.toString()));
            settings.specFile = cmd.getOptionValue(OptionString.SPECFILE.toString());
        } else {
            logger.fine("[OPTION] No Spec File included");
        }

        if (cmd.hasOption(OptionString.SPECEXT.toString())) {
            logger.fine("[OPTION] Using a specific SPEC File Extension: "
                    + cmd.getOptionValue(OptionString.SPECEXT.toString()));
            settings.specFileExt = cmd.getOptionValue(OptionString.SPECEXT.toString());
        } else {
            logger.fine("[OPTION] Using the default SPEC File Extension: " + settings.specFileExt);
        }

        // Load more than one file from the SPEC File?
        if (cmd.hasOption(OptionString.BULK.toString())) {
            logger.fine("[OPTION] Bulk SPEC File inclusion: Enabled");
            settings.bulk = true;
        } else {
            logger.fine("[OPTION] Bulk SPEC File inclusion: Disabled");
        }

        // Set the Mode of Operation for Converting SPEC to SMV
        if (cmd.hasOption(OptionString.MODE.toString())) {
            switch (cmd.getOptionValue(OptionString.MODE.toString())) {
                case "bmc" :
                    settings.mode = 1;
                    break;
                case "smc" :
                    settings.mode = 2;
                    break;
                case "both" :
                    settings.mode = 3;
                    break;
                default :
                    logger.severe("[ERROR] Unknown mode: '" + cmd.getOptionValue(OptionString.MODE.toString()) + "'");
            }
        }

        // Load more than one file from the SPEC File?
        if (cmd.hasOption(OptionString.TIMEOUT.toString())) {
            logger.fine("[OPTION] Timeout: " + cmd.getOptionValue(OptionString.TIMEOUT.toString()) + " seconds");
            settings.TIMEOUT_SECONDS = new Long(cmd.getOptionValue(OptionString.TIMEOUT.toString()));
        } else {
            logger.fine("[OPTION] Bulk SPEC File inclusion: Disabled");
        }
    }

    private void setupUserPreferenceOptions(CommandLine cmd, Options options) {
        // Set the Console's Max Width
        if (cmd.hasOption(OptionString.MAXW.toString())) {
            logger.fine("[OPTION] Setting the console's maximum width");
            String maxw = "";
            try {
                maxw = cmd.getOptionValue(OptionString.MAXW.toString());
                ((MohawkConsoleFormatter) consoleHandler.getFormatter()).maxWidth = Integer.decode(maxw);
            } catch (NumberFormatException e) {
                logger.severe("[ERROR] Could not decode 'maxw': " + maxw + ";\n" + e.getMessage());
            }
        } else {
            logger.fine("[OPTION] Default Console Maximum Width Used");
        }

        // Set the Console's Wrap String
        if (cmd.hasOption(OptionString.LINESTR.toString())) {
            logger.fine("[OPTION] Setting the console's new line string");
            ((MohawkConsoleFormatter) consoleHandler.getFormatter()).newLineStr = cmd
                    .getOptionValue(OptionString.LINESTR.toString());
        } else {
            logger.fine("[OPTION] Default Line String Used");
        }

    }

    private void setupLoggerOptions(CommandLine cmd, Options options) throws SecurityException, IOException {
        // Logging Level
        logger.setUseParentHandlers(false);
        consoleHandler.setFormatter(new MohawkConsoleFormatter());
        setLoggerLevel(Level.INFO);// Default Level
        if (cmd.hasOption(OptionString.LOGLEVEL.toString())) {
            String loglevel = cmd.getOptionValue(OptionString.LOGLEVEL.toString());
            if (loglevel.equalsIgnoreCase("quiet")) {
                setLoggerLevel(Level.SEVERE);
            } else if (loglevel.equalsIgnoreCase("warning")) {
                setLoggerLevel(Level.WARNING);
            } else if (loglevel.equalsIgnoreCase("info")) {
                setLoggerLevel(Level.INFO);
            } else if (loglevel.equalsIgnoreCase("debug")) {
                setLoggerLevel(Level.FINEST);
            } else if (loglevel.equalsIgnoreCase("fine")) {
                setLoggerLevel(Level.FINE);
            }
        }

        logger.setLevel(settings.LoggerLevel);
        consoleHandler.setLevel(settings.LoggerLevel);
        logger.addHandler(consoleHandler);

        // Add CSV File Headers
        if (cmd.hasOption(OptionString.NOHEADER.toString())) {
            settings.WriteCSVFileHeader = false;
        }

        // Set Logger Folder
        if (cmd.hasOption(OptionString.LOGFOLDER.toString())) {
            File logfile = new File(cmd.getOptionValue(OptionString.LOGFOLDER.toString()));

            if (!logfile.exists()) {
                logfile.mkdir();
            }

            if (logfile.isDirectory()) {
                settings.Logger_folderpath = cmd.getOptionValue(OptionString.LOGFOLDER.toString());
            } else {
                logger.severe("logfolder did not contain a folder that exists or that could be created!");
            }
        }

        // Set File Logger
        if (cmd.hasOption(OptionString.LOGFILE.toString())) {
            // Check if no log file was requested
            if (cmd.getOptionValue(OptionString.LOGFILE.toString()).equals("n")) {
                // Create no log file
                settings.Logger_filepath = "";
            } else if (cmd.getOptionValue(OptionString.LOGFILE.toString()).equals("u")) {
                // Create a unique log file
                settings.Logger_filepath = "mohawk-log.%u.%g.txt";
            } else {
                try {
                    // Create a log file with a specific name
                    File logfile = new File(settings.Logger_folderpath + File.separator
                            + cmd.getOptionValue(OptionString.LOGFILE.toString()));

                    if (!logfile.exists()) {
                        logfile.createNewFile();
                    }
                    settings.Logger_filepath = logfile.getAbsolutePath();

                    if (settings.WriteCSVFileHeader) {
                        FileOutputStream writer = new FileOutputStream(logfile, true);// Always append!
                        writer.write(MohawkCSVFileFormatter.csvHeaders().getBytes());
                        writer.flush();
                        writer.close();
                    }

                } catch (IOException e) {
                    logger.severe(e.getMessage());
                    return;
                }
            }
        }
        // Add Logger File Handler
        if (!settings.Logger_filepath.isEmpty()) {
            File f = new File(settings.Logger_folderpath + File.separator);
            f.mkdirs();
            fileHandler = new FileHandler(settings.Logger_folderpath + File.separator + settings.Logger_filepath, true);
            fileHandler.setLevel(getLoggerLevel());
            fileHandler.setFormatter(new MohawkCSVFileFormatter());
            logger.addHandler(fileHandler);
        }
    }

    private Boolean setupReturnImmediatelyOptions(CommandLine cmd, Options options) throws Exception {
        if (cmd.hasOption(OptionString.HELP.toString()) == true || cmd.getOptions().length < 1) {
            printHelp(cmd, options);
            return true;
        }

        if (cmd.hasOption(OptionString.VERSION.toString())) {
            // keep it as simple as possible for the version
            System.out.println(VERSION);
            return true;
        }

        if (cmd.hasOption(OptionString.NUSMVPATH.toString())) {
            logger.info("Running with the 'nusmv' Custom Configuration. " + "Changing the path from '"
                    + settings.getNuSMV_filepath() + "' to '" + cmd.getOptionValue(OptionString.NUSMVPATH.toString())
                    + "'");
            settings.setNuSMV_filepath(cmd.getOptionValue(OptionString.NUSMVPATH.toString()));
        }
        if (cmd.hasOption(OptionString.CHECKNUSMV.toString())) {
            try {
                logger.fine("[OPTION] Checking the NuSMV version number");
                String[] commands = {settings.getNuSMV_filepath(), "-help"};
                ProcessBuilder pb = new ProcessBuilder(commands);
                pb.redirectErrorStream(true);// REQUIRED: NuSVM uses STDERR
                // will throw error if it cannot find NuSMV
                Process proc = pb.start();

                BufferedReader br = new BufferedReader(new InputStreamReader(proc.getInputStream()));

                String strLine = null;
                if (br.ready()) {
                    while ((strLine = br.readLine()) != null) {
                        if (strLine.contains("This is NuSMV")) {
                            logger.info(strLine);
                            break;
                        }
                    }
                } else {
                    logger.warning("No output was given by NuSMV, " + "maybe the commandline arguments have changed "
                            + "or the file " + settings.getNuSMV_filepath() + " points to the wrong file");
                }
            } catch (IOException e) {
                logger.severe("No Version of NuSMV was found, " + "please check that NuSMV is on the PATH.");
                return true;
            }
        }

        return false;
    }

    private void setupResultOptions(CommandLine cmd, Options options) {
        if (cmd.hasOption(OptionString.RESULTSFILE.toString())) {
            logger.fine("[OPTION] Changing the results file");
            settings.testingResultsFile = cmd.getOptionValue(OptionString.RESULTSFILE.toString());
        }
        logger.info("Results File: " + settings.testingResultsFile);
    }
}
