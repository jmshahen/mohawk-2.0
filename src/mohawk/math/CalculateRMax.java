/**
 * 
 */
package mohawk.math;

import java.util.HashSet;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Logger;

import mohawk.global.pieces.mohawk.CAEntry;
import mohawk.rbac.RBACInstance;

/** @author kjayaram */
public class CalculateRMax {
    public final static Logger logger = Logger.getLogger("mohawk");

    RBACInstance rbac_instance;

    public CalculateRMax(RBACInstance in_rbac) {
        rbac_instance = in_rbac;
    }

    /*
     * Calculate RMax for a given query role
     */
    public Set<String> CalRMax() {

        Vector<CAEntry> vCAE = rbac_instance.getCA().get(rbac_instance.getSpec().get(1));
        Set<String> RMaxRoles = new HashSet<String>();
        int Max = 0;

        for (int i = 0; i < vCAE.size(); i++) {
            Set<String> setRMax = CalculateSi(vCAE.get(i));
            int nxtRMax = setRMax.size();
            if (nxtRMax > Max) {
                Max = nxtRMax;
                RMaxRoles = setRMax;
            }
        }

        return RMaxRoles;
    }

    /*
     * Calculate set of CA rules related to a CA rule as follows: We set Si <- {ai}. We then iterate as follows till we
     * reach a fix point. If the target role of a can assign rule a is a positive precondition role of some rule in Si,
     * then we add a to Si. The time-complexity of constructing Si is O(|R| x |can assign|), which is polynomial in the
     * input. A fix point exists, because a can assign rule is added at most once.
     */
    public Set<String> CalculateSi(CAEntry tmpCAE) {

        Set<CAEntry> SetOfRelCARules = new HashSet<CAEntry>();
        Set<CAEntry> SetOfCARules = GetCARuleSet();
        int PrevNumCARules = 0;

        SetOfRelCARules.add(tmpCAE);

        do {
            PrevNumCARules = SetOfRelCARules.size();
            for (CAEntry tmpCAE1 : SetOfCARules) {
                Set<CAEntry> tmpSet = new HashSet<CAEntry>();
                for (CAEntry tmpCAE2 : SetOfRelCARules) {
                    if (IsCARuleRelated(tmpCAE1, tmpCAE2)) {
                        tmpSet.add(tmpCAE1);
                    }
                }
                SetOfRelCARules.addAll(tmpSet);
                tmpSet.clear();
            }
        } while (PrevNumCARules < SetOfRelCARules.size());

        return CalculateRMaxi(SetOfRelCARules);
    }

    // Calculate the the RMax_i for each set of CA rules Si
    public Set<String> CalculateRMaxi(Set<CAEntry> SetOfCARules) {

        Set<String> SetOfRoles = new HashSet<String>();

        for (CAEntry cae : SetOfCARules) {
            SetOfRoles.add(cae.getRole());
            for (int roleindex : cae.getPreConditions().keySet()) {
                if (cae.getPreConditions().getConditional(roleindex) == 1) {
                    SetOfRoles.add(rbac_instance.getRoles().get(roleindex));
                }
            }
        }

        return SetOfRoles;
    }

    // Check if the target role in tmpCAE1 appears as a positive precondition in
    // tmpCAE2
    private boolean IsCARuleRelated(CAEntry tmpCAE1, CAEntry tmpCAE2) {
        int roleindex = 0;
        try {
            roleindex = rbac_instance.getRoleIndex(tmpCAE1.getRole());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (tmpCAE2.getPreConditions().keySet().contains(roleindex)) {
            if (tmpCAE2.getPreConditions().getConditional(roleindex) == 1) return true;
        }

        return false;
    }

    // Get a union of all CA rules from CA ruleset contained in a MAP.
    public Set<CAEntry> GetCARuleSet() {

        Set<CAEntry> SetOfCARules = new HashSet<CAEntry>();

        for (String nxtRole : rbac_instance.getCA().keySet()) {
            Vector<CAEntry> nxtVCAE = rbac_instance.getCA().get(nxtRole);
            for (int i = 0; i < nxtVCAE.size(); i++) {
                SetOfCARules.add(nxtVCAE.get(i));
            }
        }

        return SetOfCARules;
    }
}
