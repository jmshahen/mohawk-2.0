/**
 * 
 */
package mohawk.math;

import java.util.*;
import java.util.logging.Logger;

import mohawk.global.pieces.mohawk.*;
import mohawk.rbac.RBACInstance;

/** @author kjayaram */
public class CalculateRplus {
    public final static Logger logger = Logger.getLogger("mohawk");

    RBACInstance rbac_instance;
    Set<String> R_plus;
    Set<String> R_minus;

    public CalculateRplus(RBACInstance in_rbac) {
        rbac_instance = in_rbac;
        CalculateSets();
    }

    public Set<String> CalIrrevocableRoles() {

        // int iRevRoles = rbac_instance.getNumRoles() - rbac_instance.getCR().keySet().size(); // NEVER USED
        Set<String> RevocableRoles = new HashSet<String>();
        Set<String> IrrevocableRoles = new HashSet<String>(rbac_instance.getRoles());

        for (String strRole : rbac_instance.getCR().keySet()) {
            Vector<CREntry> vCRE = rbac_instance.getCR().get(strRole);
            for (int i = 0; i < vCRE.size(); i++) {
                CREntry cre = vCRE.get(i);
                if (cre.getAdminRole().equals("FALSE")) {
                    // iRevRoles++; // NEVER USED
                } else {
                    RevocableRoles.add(cre.getTargetRole());
                }
            }
        }

        IrrevocableRoles.removeAll(RevocableRoles);
        return IrrevocableRoles;
    }

    public void CalculateSets() {

        Map<String, Vector<CAEntry>> mCA = rbac_instance.getCA();
        HashSet<Integer> tmpR_plus = new HashSet<Integer>();
        HashSet<Integer> tmpR_minus = new HashSet<Integer>();

        Iterator<String> it = mCA.keySet().iterator();
        while (it.hasNext()) {
            String strTgtRole = it.next();
            Vector<CAEntry> vCA = mCA.get(strTgtRole);

            for (int i = 0; i < vCA.size(); i++) {
                CAEntry cae = vCA.get(i);
                PreCondition precond = cae.getPreConditions();

                for (int roleindex : precond.keySet()) {
                    if (precond.getConditional(roleindex) == 1) tmpR_plus.add(roleindex);
                    else tmpR_minus.add(roleindex);
                }
            }
        }

        HashSet<Integer> R_plus = tmpR_plus;
        HashSet<Integer> R_minus = tmpR_minus;
        R_plus.removeAll(tmpR_minus);
        R_minus.removeAll(tmpR_plus);

        this.R_plus = getRoleSet(R_plus);
        this.R_minus = getRoleSet(R_minus);
    }

    Set<String> getRoleSet(Set<Integer> intRoleSet) {

        HashSet<String> strRoleSet = new HashSet<String>();

        Iterator<Integer> itr = intRoleSet.iterator();
        while (itr.hasNext()) {
            int iRoleNum = itr.next();
            strRoleSet.add(rbac_instance.getRoles().get(iRoleNum));
        }

        return strRoleSet;
    }

    public Set<String> getRPlus() {
        return this.R_plus;
    }

    public Set<String> getRMinus() {
        return this.R_minus;
    }
}
