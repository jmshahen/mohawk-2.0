/**
 * 
 */
package mohawk.math;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

import mohawk.rbac.RBACInstance;
import mohawk.rbac.RBACSpecReader;

/** @author kjayaram */
public class CalculateDiameter {
    public final static Logger logger = Logger.getLogger("mohawk");

    // public CalculateDiameter() {}

    public static void main(String args[]) throws IOException {

        if (args.length < 1) {
            System.out.println("Usage: java -jar ./rbac2smv.jar CalculateDiameter rbacspec");
        }

        File rbacfile = new File(args[0]);

        if (!rbacfile.exists()) {
            System.out.println("The RBAC specification file " + rbacfile + " does not exists.");
            return;
        }

        /*
         * Reader reader = null; try { reader = new FileReader(rbacfile); } catch (FileNotFoundException e) {
         * Auto-generated catch block e.printStackTrace(); }
         */
        // RBACLexer lexer = new RBACLexer(reader);
        // RBACParser parser = new RBACParser(lexer);
        RBACInstance rbac = null;

        RBACSpecReader rbacreader = new RBACSpecReader(args[0]);
        rbac = rbacreader.getRBAC();

        CalculateRplus crplus = new CalculateRplus(rbac);
        CalculateRplusOnly crplusonly = new CalculateRplusOnly(rbac);
        // CalculateRMax rmax = new CalculateRMax(rbac);
        Set<String> R_plus = crplus.getRPlus();
        Set<String> R_minus = crplus.getRMinus();
        Set<String> RevRoles = crplus.CalIrrevocableRoles();
        Set<String> R_plusonly = crplusonly.getRplusOnly();
        // Set<String> RMaxRoles = rmax.CalRMax();
        Set<String> RUnionRoles = new HashSet<String>();
        RUnionRoles.addAll(R_plus);
        RUnionRoles.addAll(R_minus);
        RUnionRoles.addAll(RevRoles);
        RUnionRoles.addAll(R_plusonly);

        System.out.println("Tightening 1 - R- : " + R_minus.size());
        System.out.println("Tightening 2 - R+ : " + R_plus.size());
        System.out.println("Tightening 3 - R_p : " + R_plusonly.size());
        System.out.println("Tightening 4 - R_irrev : " + RevRoles.size());
        // System.out.println("Tightening 5 - R_max : " + RMaxRoles.size());
        System.out.println("Union of Tightenings : " + RUnionRoles.size());
    }

    public static int GetDiameter(RBACInstance rbacinstance) {
        RBACInstance rbac = rbacinstance;
        int no_roles = rbac.getNumRoles();
        CalculateRplus crplus = new CalculateRplus(rbac);
        CalculateRplusOnly crplusonly = new CalculateRplusOnly(rbac);
        // CalculateRMax rmax = new CalculateRMax(rbac);
        Set<String> R_plus = crplus.getRPlus();
        Set<String> R_minus = crplus.getRMinus();
        Set<String> RevRoles = crplus.CalIrrevocableRoles();
        Set<String> R_plusonly = crplusonly.getRplusOnly();
        // Set<String> RMaxRoles = rmax.CalRMax();
        Set<String> RUnionRoles = new HashSet<String>();
        RUnionRoles.addAll(R_plus);
        RUnionRoles.addAll(R_minus);
        RUnionRoles.addAll(RevRoles);
        RUnionRoles.addAll(R_plusonly);

        // System.out.println("Tightening 1 - R- : " + R_minus.size());
        // System.out.println("Tightening 2 - R+ : " + R_plus.size());
        // System.out.println("Tightening 3 - R_p : " + R_plusonly.size());
        // System.out.println("Tightening 4 - R_irrev : " + RevRoles.size());
        // System.out.println("Tightening 5 - R_max : " + RMaxRoles.size());
        int tightenings = RUnionRoles.size();

        return ((int) Math.pow(2, (no_roles - tightenings)) - 1 + tightenings);
    }

    public static Map<String, Integer> CalDiameterFile(String filename) throws IOException {

        File rbacfile = new File(filename);
        return CalDiameterFile(rbacfile);
    }

    public static Map<String, Integer> CalDiameterFile(File rbacfile) throws IOException {
        if (!rbacfile.exists()) {
            System.out.println("The RBAC specification file " + rbacfile + " does not exists.");
            return null;
        }

        /*
         * Reader reader = null; try { reader = new FileReader(rbacfile); } catch (FileNotFoundException e) {
         * Auto-generated catch block e.printStackTrace(); }
         */
        // RBACLexer lexer = new RBACLexer(reader);
        // RBACParser parser = new RBACParser(lexer);
        RBACInstance rbac = null;

        RBACSpecReader rbacreader = new RBACSpecReader(rbacfile.getAbsolutePath());
        rbac = rbacreader.getRBAC();
        int no_roles = rbac.getNumRoles();

        CalculateRplus crplus = new CalculateRplus(rbac);
        CalculateRplusOnly crplusonly = new CalculateRplusOnly(rbac);
        // CalculateRMax rmax = new CalculateRMax(rbac);
        Set<String> R_plus = crplus.getRPlus();
        Set<String> R_minus = crplus.getRMinus();
        Set<String> RevRoles = crplus.CalIrrevocableRoles();
        Set<String> R_plusonly = crplusonly.getRplusOnly();
        // Set<String> RMaxRoles = rmax.CalRMax();
        Set<String> RUnionRoles = new HashSet<String>();
        RUnionRoles.addAll(R_plus);
        RUnionRoles.addAll(R_minus);
        RUnionRoles.addAll(RevRoles);
        RUnionRoles.addAll(R_plusonly);

        System.out.println("Tightening 1 - R- : " + R_minus.size());
        System.out.println("Tightening 2 - R+ : " + R_plus.size());
        System.out.println("Tightening 3 - R_p : " + R_plusonly.size());
        System.out.println("Tightening 4 - R_irrev : " + RevRoles.size());
        // System.out.println("Tightening 5 - R_max : " + RMaxRoles.size());
        System.out.println("Union of Tightenings : " + RUnionRoles.size());

        int tightenings = RUnionRoles.size();
        int diameter = ((int) Math.pow(2, (no_roles - tightenings)) - 1 + tightenings);

        Map<String, Integer> ret = new HashMap<String, Integer>();
        ret.put("Diameter", diameter);
        ret.put("NumRoles", no_roles);
        ret.put("NumTightenings", RUnionRoles.size());
        return ret;
    }
}
