/**
 * The MIT License
 * 
 * Copyright (c) 2010 Karthick Jayaraman
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */

package mohawk.math;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Logger;

import mohawk.collections.RoleDepTree;
import mohawk.global.pieces.mohawk.CAEntry;
import mohawk.global.pieces.mohawk.PreCondition;
import mohawk.rbac.RBACInstance;

/** @author kjayaram */
public class EstimateDia {
    public final static Logger logger = Logger.getLogger("mohawk");

    RBACInstance rbac_instance;
    RoleDepTree rdeptree;

    public EstimateDia(RBACInstance in_rbac) {
        rbac_instance = in_rbac;
    }

    public int CalculateR_Minus() {

        Map<String, Vector<CAEntry>> mCA = rbac_instance.getCA();
        HashSet<Integer> tmpR_plus = new HashSet<Integer>();
        HashSet<Integer> tmpR_minus = new HashSet<Integer>();

        Iterator<String> it = mCA.keySet().iterator();
        while (it.hasNext()) {
            String strTgtRole = it.next();
            Vector<CAEntry> vCA = mCA.get(strTgtRole);

            for (int i = 0; i < vCA.size(); i++) {
                CAEntry cae = vCA.get(i);
                PreCondition precond = cae.getPreConditions();

                for (int roleindex : precond.keySet()) {
                    if (precond.getConditional(roleindex) == 1) tmpR_plus.add(roleindex);
                    else tmpR_minus.add(roleindex);
                }
            }
        }

        HashSet<Integer> R_plus = tmpR_plus;
        HashSet<Integer> R_minus = tmpR_minus;
        R_plus.removeAll(tmpR_minus);
        R_minus.removeAll(tmpR_plus);

        return R_minus.size();
    }

    public int CalculateR_Plus() {

        Map<String, Vector<CAEntry>> mCA = rbac_instance.getCA();
        HashSet<Integer> tmpR_plus = new HashSet<Integer>();
        HashSet<Integer> tmpR_minus = new HashSet<Integer>();

        Iterator<String> it = mCA.keySet().iterator();
        while (it.hasNext()) {
            String strTgtRole = it.next();
            Vector<CAEntry> vCA = mCA.get(strTgtRole);

            for (int i = 0; i < vCA.size(); i++) {
                CAEntry cae = vCA.get(i);
                PreCondition precond = cae.getPreConditions();

                for (int roleindex : precond.keySet()) {
                    if (precond.getConditional(roleindex) == 1) tmpR_plus.add(roleindex);
                    else tmpR_minus.add(roleindex);
                }
            }
        }

        HashSet<Integer> R_plus = tmpR_plus;
        HashSet<Integer> R_minus = tmpR_minus;
        R_plus.removeAll(tmpR_minus);
        R_minus.removeAll(tmpR_plus);

        return R_plus.size();
    }

    public int CalculateR_p(String strRole) {

        HashSet<String> PosDeps = getAllPosDeps(strRole);
        HashSet<String> NegDeps = getAllNegDeps(strRole);
        HashSet<String> hsIntersect = PosDeps;

        hsIntersect.containsAll(PosDeps);
        PosDeps.removeAll(hsIntersect);
        NegDeps.removeAll(hsIntersect);

        return 0;
    }

    public HashSet<String> getAllNegDeps(String strRole) {

        Vector<String> vWorkQueue = new Vector<String>();
        HashSet<String> NegDeps = new HashSet<String>();

        vWorkQueue.add(strRole);

        while (!vWorkQueue.isEmpty()) {
            String strNextRole = vWorkQueue.firstElement(); // Get first element
                                                            // from queue.
            vWorkQueue.remove(strNextRole);

            Vector<String> vTmpPosDeps = new Vector<String>();
            Vector<String> vTmpNegDeps = new Vector<String>();

            getPosDeps(vTmpPosDeps, vTmpNegDeps, strNextRole);

            Iterator<String> itrNeg = vTmpNegDeps.iterator();
            while (itrNeg.hasNext()) {
                String tmpRole = itrNeg.next();
                if (!NegDeps.contains(tmpRole)) {
                    vWorkQueue.add(tmpRole);
                    NegDeps.add(tmpRole);
                }
            }
        }

        return NegDeps;
    }

    public HashSet<String> getAllPosDeps(String strRole) {

        Vector<String> vWorkQueue = new Vector<String>();
        HashSet<String> PosDeps = new HashSet<String>();

        vWorkQueue.add(strRole);

        while (!vWorkQueue.isEmpty()) {
            String strNextRole = vWorkQueue.firstElement(); // Get first element
                                                            // from queue.
            vWorkQueue.remove(strNextRole);

            Vector<String> vTmpPosDeps = new Vector<String>();
            Vector<String> vTmpNegDeps = new Vector<String>();

            getPosDeps(vTmpPosDeps, vTmpNegDeps, strNextRole);

            Iterator<String> itrPos = vTmpPosDeps.iterator();
            while (itrPos.hasNext()) {
                String tmpRole = itrPos.next();

                if (!PosDeps.contains(tmpRole)) {
                    PosDeps.add(tmpRole);
                    vWorkQueue.add(tmpRole);
                }
            }
        }

        return PosDeps;
    }

    /* Helper function for getAllPosDeps */
    public void getPosDeps(Vector<String> vPosDeps, Vector<String> vNegDeps, String strRole) {

        Vector<CAEntry> vMatchingCA = rbac_instance.getCA().get(strRole);

        if (vMatchingCA == null) return;

        for (int i = 0; i < vMatchingCA.size(); i++) {
            CAEntry cae = vMatchingCA.get(i);
            PreCondition pcPreCond = cae.getPreConditions();

            if (pcPreCond.size() != 0) {
                for (int roleindex : pcPreCond.keySet()) {
                    String strDepRole = rbac_instance.getRoles().get(roleindex);

                    if (pcPreCond.getConditional(roleindex) == 1) vPosDeps.add(strDepRole);
                    else vNegDeps.add(strDepRole);
                }
            }
        }
    }

    public int IrrevicableRoles() {

        int num_roles = rbac_instance.getNumRoles();
        int num_revokable_roles = rbac_instance.getCR().size();

        return num_roles - num_revokable_roles;
    }

}
