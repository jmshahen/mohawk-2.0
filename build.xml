<!--
	@author Karthick Jayaraman, Jonathan Shahen
-->
<project name="mohawk-2.0" default="dist" basedir=".">
	<description>Mohawk Tool Implementation.</description>

	<!-- Properties for the JAR file -->
	<property name="user.name" location="Jonathan Shahen" />

	<!-- set global properties for this build -->
	<property name="src" location="src" />
	<property name="lib" location="lib" />
	<property name="bin" location="bin" />
	<property name="data" location="data" />
	<property name="logs" location="logs" />
	<property name="build" location="${bin}/build" />
	<property name="dist" location="${bin}/dist" />
	<property name="qtest" location="${data}/testcases" />
	<property name="mohawksrc" location="${src}/mohawk" />
	<property name="template" location="${mohawksrc}/output" />
	<property name="antlr" location="${lib}/antlr-4.4-complete.jar" />
	<property name="junit" location="${lib}/junit-4.12.jar" />
	
	<property name="parser-package" value="mohawk.rbac.generated" />

	<!-- Customize this line to point to the 'Mohawk-T Globals' Project's source folder -->
	<property name="src.globals" location="../Mohawk-T Globals/src" />
	<property name="src.globals.parsers" location="../Mohawk-T Globals/parsers" />

	<path id="test.classpath">
		<pathelement location="${build}" />
		<pathelement location="${junit}" />
		<fileset dir="${lib}">
			<include name="**/*.jar" />
		</fileset>
	</path>

	<target name="init">
		<!-- create the bin directory -->
		<mkdir dir="${bin}" />
		<!-- create the build directory -->
		<mkdir dir="${build}" />
		<!-- create the distribution directory -->
		<mkdir dir="${dist}" />
		<!-- create the logs directory -->
		<mkdir dir="${logs}" />
	</target>

	<target name="compile" depends="init" description="compile the source ">
		<!-- compile the java code from ${src} into ${build} -->
		<javac srcdir="${src}" destdir="${build}" debug="on" deprecation="true">
			<src path="${src}" />
			<src path="${src.globals}" />
			<src path="${src.globals.parsers}" />
			<classpath>
				<fileset dir="${lib}">
					<include name="**/*.jar" />
				</fileset>
			</classpath>
		</javac>
		<copy file="${template}/smvtemplatev1.st" todir="${build}/mohawk/output" />
		<copy file="${template}/smvtemplatev2.st" todir="${build}/mohawk/output" />
		<copy file="${template}/transitionsv1.st" todir="${build}/mohawk/output" />
		<copy file="${template}/transitionsv2.st" todir="${build}/mohawk/output" />
		<copy file="${template}/rbac.st" todir="${build}/mohawk/output" />
	</target>

	<target name="javadoc">
		<javadoc access="private" author="true" classpath="lib/antlr-3.2.jar;lib/stringtemplate.jar;lib/hamcrest-core-1.3.jar;lib/junit-4.12.jar;lib/commons-cli-1.2.jar;lib/commons-lang3-3.3.2.jar" destdir="javadoc" nodeprecated="false" nodeprecatedlist="false" noindex="false" nonavbar="false" notree="false" packagenames="mohawk.logging,mohawk.refine,mohawk.slicer,mohawk.util,mohawk.output,mohawk.nusmv,mohawk.testing,mohawk.math,mohawk.rbac,mohawk.collections,mohawk.helper,mohawk.rbac.generated,mohawk,mohawk.pieces" source="1.8" sourcepath="src" splitindex="true" use="true" version="true" />
	</target>

	<target name="dist" depends="compile" description="generate the distribution">
		<!-- put everything in ${build} into the jar file -->
		<jar destfile="${dist}/${ant.project.name}.jar" basedir="${build}">
			<manifest>
				<attribute name="Built-By" value="${user.name}" />
				<attribute name="Main-Class" value="mohawk.Mohawk" />
				<attribute name="Class-Path" value="." />
			</manifest>
			<fileset dir="${build}" />
			<zipfileset src="${lib}/stringtemplate.jar" />
			<zipfileset src="${antlr}" />
			<zipfileset src="${lib}/commons-cli-1.2.jar" />
			<zipfileset src="${lib}/commons-lang3-3.3.2.jar" />
			<zipfileset src="${lib}/hamcrest-core-1.3.jar" />
			<zipfileset src="${lib}/junit-4.12.jar" />
		</jar>
	</target>

	<target name="qtest" depends="dist" description="generate a JAR file that will be placed close to the data for quick testing">
		<!-- @author Jonathan Shahen -->
		<!-- put everything in ${build} into the jar file -->
		<copy file="${dist}/${ant.project.name}.jar" tofile="${qtest}" />
	</target>

	<target name="regression" depends="dist" description="Performs a regression test">
		<!-- @author Jonathan Shahen -->
		<junit fork="yes" dir="." haltonfailure="true" printsummary="true">
			<test name="mohawk.test.MohawkRegressionTests" />
			<formatter type="plain" usefile="false" />
			<classpath refid="test.classpath" />
		</junit>

		<!-- OLD Windows ONLY running JUnit through the commandline
		<exec executable="cmd" dir="${dist}" failonerror="true">
			<arg value="/c" />
			<arg value="java -jar mohawk.jar -mode bmc -run all -rbacspec ../../data/regressiontests/positive1.spec" />
		</exec>
		-->
	</target>

	<target name="clean" description="clean up">
		<!-- delete the ${build} and ${dist} directory trees -->
		<delete dir="${bin}" />
		<delete>
			<fileset dir="." defaultexcludes="no">
				<include name="**/*.*~" />
			</fileset>
		</delete>
	</target>
</project>
