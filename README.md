/********************************************************************
 * PROGRAM NAME: Mohawk - A Model Checker for ARBAC Policies	
 *		
 * AUTHORS: Jonathan Shahen; Karthick Jayaraman
 *	
 * BEGIN DATE: October, 2009
 *
 * LICENSE: Please view LICENSE file in the home dir of this Program
 ********************************************************************/

All of the documentation is very old, please use the ANT build script,
and run `mohawk.jar --help` to see all available options.

NuSMV version should be 2.6
Java version should be JRE8.0+

INSTALL
-------
See INSTALL file in the home directory of this program

LICENSE
-------
See LICENSE file in the home directory of this program

WEBSITE
-------
https://ece.uwaterloo.ca/~jmshahen/mohawk+t/

Bitbucket REPOSITORY
----------------------
git clone https://bitbucket.org/jshahen/mohawk-2.0.git


