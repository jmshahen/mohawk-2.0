/* Generated On        : 2015/02/07 20:24:43.536
 * Generated With      : Mohawk Reverse Converter: ASAPTime NSA
 * Number of Roles     : 15
 * Number of Timeslots : 30
 * Number of Rules     : 552
 * 
 * Roles     : |Size=15| [role1, role2, role3, role4, role5, role6, role7, role8, role9, role10, role11, role12, role13, role14, role15]
 * Timeslots : |Size=30| [t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15, t16, t17, t18, t19, t20, t21, t22, t23, t24, t25, t26, t27, t28, t29, t30]
 * 
 * 
 * TESTCASE COMMENTS:
 * 
 */

Query: t2, [role3]

Expected: REACHABLE

/* 
 * Number of Rules       : 282
 * Largest Precondition  : 2
 * Largest Role Schedule : 1
 * Startable Rules       : 200
 * Truly Startable Rules : 36
 */
CanAssign {
    /* 259 */  <TRUE, t27, NOT ~ role12, t2, role3>
}

/* 
 * Number of Rules       : 134
 * Largest Precondition  : 0
 * Largest Role Schedule : 1
 * Startable Rules       : 134
 * Truly Startable Rules : 25
 */
CanRevoke {
}

/* 
 * Number of Rules       : 91
 * Largest Precondition  : 2
 * Largest Role Schedule : 1
 * Startable Rules       : 76
 * Truly Startable Rules : 16
 */
CanEnable {
}

/* 
 * Number of Rules       : 45
 * Largest Precondition  : 2
 * Largest Role Schedule : 1
 * Startable Rules       : 31
 * Truly Startable Rules : 1
 */
CanDisable {
    /*  1 */  <role3, t2, TRUE, t24, role14>
    /*  2 */  <role3, t15, TRUE, t9, role14>
    /*  3 */  <role3, t9, TRUE, t28, role14>
    /*  4 */  <role3, t3, TRUE, t6, role14>
    /*  5 */  <role3, t27, TRUE, t18, role13>
    /*  6 */  <role3, t18, TRUE, t9, role13>
    /*  7 */  <role5, t14, TRUE, t27, role12>
    /*  8 */  <role5, t13, TRUE, t2, role8>
    /*  9 */  <role5, t7, TRUE, t10, role8>
    /* 10 */  <role5, t9, TRUE, t16, role3>
    /* 11 */  <role1, t27, role12 & role3, t15, role15>
    /* 12 */  <role9, t28, TRUE, t3, role2>
    /* 13 */  <role5, t8, TRUE, t14, role4>
    /* 14 */  <role5, t20, TRUE, t24, role5>
    /* 15 */  <role3, t27, TRUE, t18, role3>
    /* 16 */  <role3, t18, TRUE, t9, role3>
    /* 17 */  <role5, t14, TRUE, t27, role2>
    /* 18 */  <role5, t13, TRUE, t2, role5>
    /* 19 */  <role5, t7, TRUE, t10, role5>
    /* 20 */  <role5, t9, TRUE, t16, role13>
    /* 21 */  <role1, t27, role12 & role3, t15, role13>
    /* 22 */  <role5, t5, NOT ~ role12, t9, role4>
    /* 23 */  <role5, t19, NOT ~ role12, t30, role4>
    /* 24 */  <role9, t18, role3 & NOT ~ role9, t6, role10>
    /* 25 */  <role9, t10, role3 & NOT ~ role9, t3, role10>
    /* 26 */  <role9, t8, role3 & NOT ~ role9, t1, role10>
    /* 27 */  <role14, t5, role9, t17, role1>
    /* 28 */  <role5, t21, TRUE, t8, role8>
    /* 29 */  <role5, t7, TRUE, t26, role8>
    /* 30 */  <role5, t30, TRUE, t4, role8>
    /* 31 */  <role5, t16, TRUE, t30, role8>
    /* 32 */  <TRUE, t19, role3, t27, role13>
    /* 33 */  <role6, t3, role3, t30, role7>
    /* 34 */  <TRUE, t23, role3, t8, role7>
    /* 35 */  <role6, t7, role8, t5, role7>
    /* 36 */  <role5, t8, NOT ~ role3, t30, role12>
    /* 37 */  <role5, t1, NOT ~ role12, t29, role3>
    /* 38 */  <TRUE, t25, NOT ~ role12, t16, role3>
    /* 39 */  <role5, t28, NOT ~ role12, t10, role3>
    /* 40 */  <role5, t5, NOT ~ role12, t9, role3>
    /* 41 */  <role5, t19, NOT ~ role12, t30, role3>
    /* 42 */  <role9, t18, role3 & NOT ~ role9, t6, role11>
    /* 43 */  <role9, t10, role3 & NOT ~ role9, t3, role11>
    /* 44 */  <role9, t8, role3 & NOT ~ role9, t1, role11>
    /* 45 */  <role14, t5, role9, t17, role10>
}